EDAN70
======

Coursework for the course EDAN70, Sweden.

Authors:

* Mattias Eklund (mat10mek@student.lu.se)
* Christian Frid (ada10cfr@student.lu.se)
* Adam Wamai Egesa (ada07awa@student.lu.se)


## Setup ##

Before you can run the program you'll need an index for the articles you want to use as a knowledgebase.

This is done by running the program wiki indexer.
wikiIndexer requires a folder of articles to be located one hierachy above. 
For example, if wikiIndexer is located as such:

*~/workspace/edan70/src/wikiIndexer.java*

the folder containing the articles should be located as such:

*~/workspace/edan70/articles/*

When the program is done you should have a index that can be used by the main program.

Download the articles here: 

* https://drive.google.com/file/d/0B-MZNNcle3zPblM5OHY0MTNMd0U/view?usp=sharing

Download the stagger model here:
* https://drive.google.com/file/d/0B-MZNNcle3zPQXFvNHJPYXEtY0U/view?usp=sharing

These jars have to be included in the projects build path for the program to work: 

* **Lucene 4.10.2 Jars**, either download the 15 we used in our project or do your own Maven, Gradle or Ant configuration.  15 jars in a zip on Google Drive: https://drive.google.com/file/d/0B1CQvFXqX-WAdzBIemhiOXBsdWc/view?usp=sharing

* **Stagger**. The one used for the project's build path is available here: https://drive.google.com/file/d/0B1CQvFXqX-WAc2tkVjdJMy1hVHc/view?usp=sharing

* Custom jars that we built ourselves. Should be optional: https://drive.google.com/folderview?id=0B1CQvFXqX-WAci1NMmdsN0ZqcEU&usp=sharing


## Run ##

After verifying that all the steps in the setup are complete the Question Answering System can be launched as followed:

1. Run the main method in *src/main/java/queryPipeline/QASystemMain.java*. Be aware that the Stagger model is first loaded into the RAM memory before the GUI is launched. -Xmx2g -Xms2g or similar run configuration is thus necessary. It will further take up to little more than a minute to load the model.
2. This opens a window with a text field, a button and a box
3. Write your question in the text field and press "Search"
4. The answers to your question are presented in decending order ("best" answer at the top)

## License ##
The license is GPLv2
