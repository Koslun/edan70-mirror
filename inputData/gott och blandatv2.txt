På Sveriges tron	250	Vad heter Sveriges drottning?	Silvia		Person
På Sveriges tron	500	Hur många barn har kungen och drottningen?	Tre		Amount
På Sveriges tron	1000	Vem ska ta över tronen efter vår kung?	Kronprinsessan Victoria		Person
På Sveriges tron	2000	En svensk kung har fått ett världsberömt skidlopp uppkallat efter sig. Vad heter det?	Vasaloppet	(efter Gustav Vasa)	Concept
På Sveriges tron	5000	Gustav VI Adolf, vår kungs farfar, sysslade bl a med historiska utgrävningar. Vad kallas det med ett annat speciellt ord?	Arkeologi      Concept
På Sveriges tron	10000	Erik XIV tros ha blivit förgiftad på order av sin bror Johan. I vilken mat ska giftet ha serverats?	Ärtsoppa		Concept
Sjörövare	250	Vilken Astrid Lindgrenfigur slåss mot sjörövare i Söderhavet?	Pippi Långstrump		Person
Sjörövare	500	Vad kan man hitta under X:et på sjörövarkartor?	En skatt		Concept
Sjörövare	1000	Vilket ord som börjar på P är också ett ord för sjörövare?	Pirat		Concept
Sjörövare	2000	Hur såg en klassisk sjörövarflagga ut?	Svart med vit dödskalle	(och korslagda benknotor)	Description
Sjörövare	5000	Hur fick sjörövarnas fångar hoppa i havet som straff?	De fick gå på plankan	(som stack ut från båten över vattnet)	Description
Sjörövare	10000	Sjörövarna var ofta ute efter gulddubloner. Vad var det?	Guldmynt		Concept
Siffror med mening	250	Hur många ljus är det oftast i en adventsljusstake med stearinljus?	4		Amount
Siffror med mening	500	Vilket betraktas som oturstalet?	13		Amount
Siffror med mening	1000	Hur många sidor har en kvadrat?	4		Amount
Siffror med mening	2000	Hur många år är ett decennium?	10		Amount
Siffror med mening	5000	Hur många år är ett millennium?	1000		Amount
Siffror med mening	10000	Hur många dagar är det på ett år då det är skottår?	366		Amount
Släkten är bäst	250	Vad kallar du din mammas pappa?	Morfar		Person	Description
Släkten är bäst	500	Vad kallas två syskon som fötts nästan samtidigt?	Tvillingar		Person	Description
Släkten är bäst	1000	Vad är din mammas syster till dig?	Moster		Person	Description
Släkten är bäst	2000	Vad är din pappas brors eller systers barn till dig?	Kusiner		Person	Description
Släkten är bäst	5000	Vad burkar man kalla ett barn som är fött många år efter sina syskon?	Sladdbarn		Person	Description
Släkten är bäst	10000	Vad kallas det med ett speciellt ord när man försöker ta reda på saker om sina förfäder långt tillbaka i tiden?	Släktforskning		er
Det gamla Egypten	250	Vad heter de spetsiga byggnader som användes som gravplatser och som står kvar än i dag?	Pyramider		Concept	Other
Det gamla Egypten	500	Vad kallar men de balsamerade lindade döda människor man hittat i pyramiderna?	Mumier		Description
Det gamla Egypten	1000	I vilken tjock bok kan man läsa om när Moses ledde Israels barn ur Egypten?	I Bibeln		Concept	Other
Det gamla Egypten	2000	Vilken titel har de egyptiska kungarna?	Farao		Person	Definition
Det gamla Egypten	5000	Vad var hieroglyfer i det gamla Egypten?	Deras skriftspråk	(bokstäver)	Concept
Det gamla Egypten	10000	En Sfinx är en speciell egyptisk staty som har lejonkropp, men vad har den för huvud?	Ett människohuvud		Concept
Riddare	250	Vad hade riddaren på kroppen för att skydda den mot skador?	En rustning		Concept
Riddare	500	Vad satt riddarna på när de stred?	Sin häst		Concept
Riddare	1000	Riddaren höll en lans i högerhanden. Vad hade han i vänsterhanden förutom tyglarna?	En sköld		Concept
Riddare	2000	Vad hade riddarna för vapen i en skida på sin vänstra sida?	Ett svärd		Concept
Riddare	5000	Riddarna hade ofta visir på hjälmen. Vad är ett visir?	Ett uppfällbart ansiktsskydd		Concept
Riddare	10000	Vad hette tävlingarna där riddarna stred mera på skoj?	Tornerspel	(torneringar)	Concept
Kungafamiljen	250	Vad kallar man den prins eller prinsessa som ska ärva tronen och bli king eller drottning?	Kronprins eller kronprinsessa		Definition
Kungafamiljen	500	Vad heter Sveriges nuvarande kung?	Carl XVI Gustav		Person
Kungafamiljen	1000	Utanför vilken storstad ligger Drottningholm där kungen och drottningen bor?	Stockholm		Location
Kungafamiljen	2000	Vad heter kungaparets ende son?	Carl Philip		Person
Kungafamiljen	5000	Drottning Silvia är delvis uppvuxen i Brasilien, men från vilket land kommer hennes familj?	Tyskland		Location
Kungafamiljen	10000	Vad är vår kungafamiljs efternamn (som sällan används av dem)?	Bernadotte		Person
Kända syskon	250	Linus och Niclas Wahlgren har en känd syster som spelar teater och sjunger. Vad heter hon?	Pernilla Wahlgren		Person
Kända syskon	500	Nämn ett av förnamnen på de två tjejer i Bubbles som är tvillingar?	Patricia eller Sandra		Person
Kända syskon	1000	Vad heter Lisa och Maggie Simpsons storebror i den tecknade serien?	Bart		Person
Kända syskon	2000	Vem är prinsessorna Margarethas, Birgittas, Désirées, och Christinas kända lillebror?	Vår kung, Carl XVI Gustav		Person
Kända syskon	5000	Vad heter de amerikanska megakända tvillingarna Mary-Kate och Ashley i efternamn?	Olsen		Person
Kända syskon	10000	Vilken sport sysslar tvillingarna Henrik och Daniel Sedin med?	Ishockey		Concept
Att sitta på	250	Vad kallas sitsen på en cykel med ett annat ord?	Sadel		Concept
Att sitta på	500	Vad kallas motsvarigheten till fåtölj men med flera sittplatser?	Soffa		Concept
Att sitta på	1000	Vad kallas en stol utan ryggstöd?	Pall		Concept
Att sitta på	2000	Vad brukar man kalla en sittplats för flera personer utan ryggstöd?	Bänk		Concept
Att sitta på	5000	Vad kallas en soffa som kan göras om till en eller flera sängar?	Bäddsoffa		Concept
Att sitta på	10000	Vad kallas en stol med rundade medar?	Gungstol		Concept
Leksaker	250	Vad kallas ett hus i miniatyr som många flickor leker med och möblerar?	Dockskåp		Concept
Leksaker	500	Vad kallas den sorts tidsfördriv där man ska foga ihop många bitar till en bild?	Pussel		Concept
Leksaker	1000	Vad heter dockan vars pojkvän heter Ken?	Barbie		Person
Leksaker	2000	Leksakstillverkaren Brio har framför allt blivit känd för att tillverka leksaker, t ex tåg, av ett visst material. Vilket?	Trä		pt
Leksaker	5000	Vilket företag är det som gör så kallade bionicle?	Lego		Organization
Leksaker	10000	En rund trissa på ett snöre anses av många vara världens äldsta leksak. Vad kallas en sådan?	Jojo		Concept
Huvudbonader	250	Vad kallas den sortens mössa som en tomte brukar bära?	Luva	(tomteluva)	Concept
Huvudbonader	500	Vad kallas den hatt man ofta använde i "Vilda Västern"?	Cowboyhatt		Concept
Huvudbonader	1000	Vad bör man ha på huvudet om man jobbar i en gruva?	En hjälm		Concept
Huvudbonader	2000	Vad kallas en sportig mössa med skärm med ett annat ord?	Keps		Concept
Huvudbonader	5000	Hur många kanter har hatten enligt en känd visa?	Tre		Amount
Huvudbonader	10000	Vad kallas den mjuka, runda mössa som Sickan i Jönssonligan brukar bära?	Basker		Concept
Klädesplagg	250	Vad heter det man sover i som består av en tröja eller jacka och ett par byxor?	Pyjamas		Concept
Klädesplagg	500	Vad är ett annat namn för kortbyxor?	Shorts		Concept
Klädesplagg	1000	Vad kallas en kortärmad bomullströja med ett engelskt uttryck?	T-shirt		Concept
Klädesplagg	2000	Vad kallas en kavaj och byxor i samma tyg, med ett speciellt ord?	Kostym		Concept
Klädesplagg	5000	Vad kallas ett ärmlöst klädesplagg med knappar eller dragkedja framtill?	Väst		Concept
Klädesplagg	10000	Vad kallas ett par byxor med hängslen och bröstlapp med ett annat ord?	Snickarbyxor		Concept
På fötterna	250	Vad är bra att ha på fötterna om det regnar?	Gummistövlar		Concept	Other
På fötterna	500	Vad kallas skor gjorda av remmar som korsas över fötterna?	Sandaler		Concept	Other
På fötterna	1000	Vad är underst på en sko?	Sulan		Concept	Other
På fötterna	2000	Vad har man på fötterna om man åker skidor?	Pjäxor		Concept	Other
På fötterna	5000	Vilken typ av skor har en 100-meterslöpare på tävling?	Spikskor		Concept	Other
På fötterna	10000	Är det damer eller herrar som bär pumps på fötterna?	Damer	(det är damers finskor med klack)	Multiplechoice
Fyll på talesättet!	250	Borta bra men hemma...?	Bäst		Other
Fyll på talesättet!	500	Man ska sluta när det är som...?	Roligast		Concept
Fyll på talesättet!	1000	Tala är silver, tiga är...?	Guld		Concept
Fyll på talesättet!	2000	Man ska inte gå över ån efter...?	Vatten		Concept
Fyll på talesättet!	5000	När katten är borta...?	Dansar råttorna på bordet		Other
Fyll på talesättet!	10000	Bättre fly än illa...?	Fäkta		Other
Djur i talesätt	250	Modig som ett...?	Lejon		Concept
Djur i talesätt	500	Stark som en...?	Björn		Concept
Djur i talesätt	1000	Hal som en...?	Ål		Concept
Djur i talesätt	2000	Listig som en...?	Räv		Concept
Djur i talesätt	5000	Envis som en..?	Åsna		Concept
Djur i talesätt	10000	Pigg som en...?	Mört		Concept
Tokig i glass	250	Vilken färg har bilarna från Hemglass?	Blå		Other
Tokig i glass	500	Cornettoglass finns inte på pinne, utan som vad?	Strutglass		Concept
Tokig i glass	1000	Vad heter den lilla ljusgröna isglassen från GB?	Piggelin		Concept
Tokig i glass	2000	Vilken figur har GB som symbol?	En clown		Person	Description
Tokig i glass	5000	Vad heter GB:s lyxpinnglass i flera smaker vars namn betyder stor på latin?	Magnum		Concept
Tokig i glass	10000	Vilket siffernamn har den klassiska vanilj- och ischokladglassen på pinne?	88:an		Concept
Läskigt	250	Vad heter den bruna läsken, på en speciell flaska, som är konkurrent med Pepsi?	Coca cola		Organization
Läskigt	500	Vilken typisk svensk brun läsk säljs extra mycket till jul och påsk?	Must	(julmust/påskmust)	Concept
Läskigt	1000	Vilken är den vanligaste smaken på cider?	Äpple		Concept
Läskigt	2000	Vilken läsk finns bl a i smakerna Citron, Vilda bär, Exotiska frukter och Fläder Citrus?	Fanta		Concept
Läskigt	5000	Vilken läsk med en engelsk sjua i namnet smakar fruktsoda?	7up	(Seven up)	Concept
Läskigt	10000	Hur många centiliter innehåller en liten läskburk?	33 cl		Amount	Other
Kryddor	250	Vilken vit krydda kan utvinnas ur havsvatten?	Salt		Concept
Kryddor	500	Vilken krydda finns i bit- eller strö- och används i kaffe och te?	Socker		Concept
Kryddor	1000	Vilken brun krydda brukar man ha på julgröten?	Kanel		Concept
Kryddor	2000	Vilken vanlig krydda finns i svart, vit och grön variant?	Peppar		Concept
Kryddor	5000	Vad heter de gula, jättedyra kryddpulvret som man har i lussekatter?	Saffran		Concept
Kryddor	10000	Vilken örtkrydda brukar ligga på köpepizzan?	Oregano		Concept
Snabbmat	250	Vilken klassisk svensk snabbmat säljs i bröd med senap och ketchup?	Varm korv		Concept
Snabbmat	500	Vilken snabbmat förknippar man med kedjor som Max och McDonalds?	Hamburgare		Concept
Snabbmat	1000	Vilken ursprunglig italiensk rätt kan heta Vesuvio eller Calzone?	Pizza		Concept
Snabbmat	2000	Vilken rätt består av tunna köttskivor från spett, grönsaker och sås i bröd?	Kebab		Concept
Snabbmat	5000	Vad kallas pizzan som säljs i små pappersförpackningar och som man värmer i microugnen tillsammans med förpackningen?	Panpizza		pt
Snabbmat	10000	Vad heter de friterade bullar av kikärter som serveras i bröd med sås?	Falafel		Concept
Butikskedjor	250	Vad heter den stora svenskan möbelvaruhus som säljer bokhyllan Billy i hela världen?	IKEA		Organization
Butikskedjor	500	Hur förkortas den svenska klädkedjan Hennes&Mauritz?	H&M		Definition
Butikskedjor	1000	Vad heter Sveriges största livsmedelskedja, med tre bokstäver i namnet?	ICA		Organization	Other
Butikskedjor	2000	Vilken butikskedja för sportkläder har en löparbana som symbol?	Stadium		Organization	Other
Butikskedjor	5000	Vad heter den danska säng- och sängklädesbutik som kallar sig för bäddlager?	Jysk		Organization	Other
Butikskedjor	10000	Vad heter den foto- och elektronikkedja som säger att de är vad de heter?	Expert		Organization	Other
Färger	250	Vilken färg har Bamse på sin luva?	Blå		Other
Färger	500	Vilka två färger är dominerande på Spidermans dräkt?	Rött och blått		Other
Färger	1000	Vilka färger har Knattarna i Kalle Anka på sina mössor?	Blå, grön, röd		Other
Färger	2000	Vilken färg har gubben i på skylten som markerar övergångsställe?	Svart		Other
Färger	5000	Vilken färg har ädelstenen rubin?	Röd		Other
Färger	10000	Vad kallas blågrön färg med ett annat ord?	Turkos		Other
I trädgården	250	Vilken vitmålad sak med guldknopp i trädgården är 8-12 meter hög?	Flaggstången		Concept	Other
I trädgården	500	Vad kallas den dela av staketet som går att stänga och öppna?	Grind		Concept	Other
I trädgården	1000	Vad kallas det när man planterar blommor tillsammans, t ex framför huset?	Rabatt		Concept	Other
I trädgården	2000	Vad kallas den rad av buskar, ofta klippta, som kan ersätta ett staket?	Häck		Concept	Other
I trädgården	5000	Vad kallas den behållare där växtdelar förmultnar?	Kompost		Concept	Other
I trädgården	10000	Vad heter den gammaldags typ av klocka där skuggan visar tiden?	Solur		Concept	Other
Vad är det gjort av?	250	Vad är chips gjort av för rotfrukt?	Potatis		Concept
Vad är det gjort av?	500	Vad är en tändsticksask gjort av?	Papper		Concept
Vad är det gjort av?	1000	Vad är en Big Pack glasslåda gjort av?	Plast		Concept
Vad är det gjort av?	2000	Vad är ett knivblad gjort av?	Stål	(järn)	Concept
Vad är det gjort av?	5000	Vad är ett cykeldäck gjort av?	Gummi		Concept
Vad är det gjort av?	10000	Från vilket djur kommer innehållet i en tagelmadrass?	Hästen	(man eller svans)	Concept
I snabbköpet	250	Var betalar man sina varor?	I kassan		Location	Other
I snabbköpet	500	Var hittar man bilar, däck och Dumle?	På godisavdelningen		Location	Other
I snabbköpet	1000	Vad är Brago, Ballerina och Marie exempel på?	Kex	(kakor)	Concept
I snabbköpet	2000	Vilket livsmedel säljs under namn som Herrgård och Grevé?	Ost		Concept
I snabbköpet	5000	Vilken sorts mat säljs under namn som Whiskas och Pussi?	Kattmat		Concept
I snabbköpet	10000	Vad handlar man i charkuteridisken?	Köttvaror		Concept
Snacks	250	Vilka snacks är upphettade majskorn?	Popcorn		Concept
Snacks	500	Vilka snacks kan man också kalla ostkrokar?	Ostbågar		Concept
Snacks	1000	Vilka snacks kan ha smak av sour cream and onion (gräddfil och lök)?	Chips		Concept
Snacks	2000	Nämn en av de två stora snackstillverkarna i Sverige?	OLW eller Estrella		Organization	Other
Snacks	5000	Vilka saltade nötter är egentligen av ärtsläktet?	Jordnötter		Concept
Snacks	10000	Vad kallas den gräddfilsbaserade kryddsås man doppar snacks i?	Dip		Concept
Drycker	250	Vilken är den allra billigaste och naturligaste dryck du kan dricka?	Vatten		Concept
Drycker	500	Vilken varm brunsvart dryck dricks ofta av vuxna?	Kaffe		Concept
Drycker	1000	Vilken dryck får man genom att doppa en påse med små växtblad i hett vatten?	Te		Concept
Drycker	2000	Vilken dryck kan man köpa i varianter som mini- , mellan-, lätt-, och gammaldags?	Mjölk		Concept
Drycker	5000	Vilken dryck kan du få vispgrädde till om det ska vara lyxigt?	Choklad		Concept
Drycker	10000	Vilken ren dryck av t ex apelsin, äpple eller ananas får du kanske till frukost ibland?	Juice		Concept
Godis	250	Vad förknippar man Marabou med för sorts godis?	Choklad		Concept
Godis	500	Vilka tre färger är det på Ahlgrens godisbilar?	Vita, gröna och rosa		Concept
Godis	1000	Hur förpackas godis som Viol, Svarta katten och PimPim?	I tablettaskar		Location	Other
Godis	2000	Vad kallas en godisbit fastsatt på en pinne av trä eller papp?	Klubba eller slickepinne		Concept
Godis	5000	I Aladdinasken finns både ljus och mörk choklad. Vad heter asken som bara innehåller ljus choklad?	Paradis		Concept
Godis	10000	Vad smakar Djungelvrål?	Saltlakrits	(supersalt!)	Concept
Väskor	250	Vad kallas en väska som man bär på ryggen, med remmar över båda axlarna?	Ryggsäck		Concept	Other
Väskor	500	Vad kallas den väska som de flesta mammor och en del flickor har sina pengar och andra småsaker i?	Handväska		Concept	Other
Väskor	1000	Vad kallas den sorts stora väska som man packar i om man t ex flyger på semester, och som kan ha hjul?	Resväska		Concept	Other
Väskor	2000	Vad kallar man ofta en lite avlång tygväska som man t ex har sina träningskläder i?	Bag		Concept	Other
Väskor	5000	Vad kallas den lilla väska man brukar ha sina toalettsaker som tandborste och tvål i när man är på resa?	Necessär	(toalettväska)	er
Väskor	10000	Vad kallas en platt väska som man förvarar jobbpapper i, och som kan ha dokument- före namnet?	Portfölj		Concept	Other
Matrecept	250	Vad är pommes frites gjorda av?	Potatis		Concept
Matrecept	500	Vilket engelskt ord använder vi för det man får om man mixar glass och mjölk och eventuellt sylt eller kakao?	Milkshake		Concept
Matrecept	1000	Om man vispar ägg och vatten och steker i stekpanna, vad får man då?	En omelett		Concept
Matrecept	2000	Vilken sorts gryn brukar ingå i recept på chokladbollar?	Havregryn		Concept
Matrecept	5000	Nämn en av de tre huvudingredienserna i pannkakssmet?	Mjöl, mjölk, ägg	(salt och margarin brukar också ingå)	Concept
Matrecept	10000	Vad kallas den pastarätt där man varvar pastaplattor, köttfärs och vit sås?	Lasagne		Concept
Korvar	250	Vad kallas en korv som ofta köps i ring, och har namn efter en stad i dalarna?	Falukorv		Concept
Korvar	500	Vad kallas de små 'kungliga' korvar som ofta finns på smörgåsbord och julbord?	Prinskorv		Concept
Korvar	1000	Vad heter Scans grillkorv vars namn betyder heta hundar på engelska?	Hot dogs		Concept
Korvar	2000	Från vilket nordiskt land kommer röda korvar?	Danmark		Location
Korvar	5000	Från vilket land kommer korven bratwurst?	Tyskland		Location
Korvar	10000	Vilken är den dominerande kryddan i salamikorv?	Vitlök		Concept
Smörgåspålägg	250	Vilket smörgåspålägg gjort av mjölk, hyvlas?	Ost		Concept
Smörgåspålägg	500	Vad kallas det söta pålägg av t ex apelsin eller aprikos som många har på rostat bröd?	Marmelad		Concept
Smörgåspålägg	1000	Vilken färg har Keso?	Vit		Concept
Smörgåspålägg	2000	Vilket smörgåspålägg består delvis av fiskägg och säljs i tub?	Kaviar		Concept
Smörgåspålägg	5000	Vilket pålägg består av mald lever uppblandad med bl a fett och kan bredas eller skivas?	Leverpastej		Concept
Smörgåspålägg	10000	Vad kallas det bredbara pålägg som kokas ihop av vassla och säljs bl a under märket Fjällbrynt?	Messmör		Concept
Gudar och gudinnor	250	Vad hette åskguden i den gamla asatron?	Tor		Person
Gudar och gudinnor	500	I vilken religion tillber man Buddha?	Buddhismen		Concept
Gudar och gudinnor	1000	Muslimerna kallar honom Allah, judarna säger Jahve. Vad heter han på svenska?	Gud		Concept
Gudar och gudinnor	2000	Vem är den störste guden i den gamla asatron?	Oden		Person
Gudar och gudinnor	5000	Vad heter den romerske kärleksguden som skjuter pilar?	Amor		Person
Gudar och gudinnor	10000	Han kan kallas Neptun eller Poseidon och brukar bära en treudd. Vad härskade denna gud över?	Havet		Concept
Månader	250	Vilken månad firar vi jul?	December		Concept
Månader	500	Vilka tre månader sjunger Gyllene tider om som också är "sommarlovsmånaderna"?	Juni, juli, augusti		Concept
Månader	1000	Vilken är årets tionde månad men med ett namn som betyder åtta?	Oktober		Concept
Månader	2000	I vilken månad firar vi midsommar?	Juni		Concept
Månader	5000	Vilken månad är uppkallad efter guden Janus med två ansikten?	Januari		Concept
Månader	10000	I vilken månad infaller vårdagjämningen?	Mars		Concept
Riksdagen	250	Vilket var det största partiet i riksdagen 2004?	Socialdemokraterna		Organization
Riksdagen	500	Vad heter det politiska parti som förkortas M?	Moderaterna		Organization
Riksdagen	1000	Hur gammal måste man vara för att få rösta i riksdagsvalet - 15, 18 eller 21 år?	18 år		Multiplechoice
Riksdagen	2000	Vilket parti förkortas mp och har en maskros som symbol?	Miljöpartiet		Organization
Riksdagen	5000	Hur ofta har vi riksdagsval i Sverige?	Vart fjärde år		Amount
Riksdagen	10000	Hur många procent röster måste ett parti få som minst för att komma in i riksdagen - 4, 5 eller 6?	4		Multiplechoice
Regeringen	250	I vilken stad sitter Sveriges regering?	Stockholm		Location
Regeringen	500	Vad kallas den högste chefen för regeringen?	Statsminister		Person
Regeringen	1000	Vilket parti har regerat mest i Sverige de senaste hundra åren?	Socialdemokraterna		Organization
Regeringen	2000	Vilken minister har hand om ekonomi och pengar?	Finansministern		Person
Regeringen	5000	Vilken minister är den som har hand om kontakterna med andra länder?	Utrikesministern		Person
Regeringen	10000	Vilken minister bestämmer över militären?	Försvarsministern		Person
Bröd	250	Vad kallas hårt bröd med ett annat ord?	Knäckebröd		Concept
Bröd	500	Vad har en baguette för form?	Den är avlång		Description
Bröd	1000	Vilket av våra fyra sädesslag ger det ljusaste brödet?	Vete		Concept
Bröd	2000	Vilken sorts frön brukar ligga ovanpå hamburgerbröd?	Sesamfrön		Concept
Bröd	5000	I vilken bön hörs orden vårt dagliga bröd, giv oss idag?	Fader vår		Concept
Bröd	10000	Från vilket medelhavsland kommer bröden ciabatta och focaccia?	Italien		Location
I mejerikylen	250	Vilken mejeriprodukt kan vispa och läggas på tårtan?	Grädde		Concept
I mejerikylen	500	Vad brukar man oftast steka i för matfett om man inte använder olja?	Margarin		Concept
I mejerikylen	1000	Vad brukar man ha till Cornflakes och Kalaspuffar om det inte är mjölk?	Fil	(filmjölk)	Concept
I mejerikylen	2000	Vad får man om man kärnar, alltså vispar grädde tillräckligt länge?	Smör		Concept
I mejerikylen	5000	Vilken mejeriprodukt finns som naturell eller smaksatt med t ex vanilj och som ofta äts till frukost?	Yoghurt		Concept
I mejerikylen	10000	Vilken sorts mejeriprodukt äter man med sill och gräslök till midsommar?	Gräddfil		Concept
Förkortningar	250	Hur förkortas den europeiska unionen?	EU		Location
Förkortningar	500	Hur förkortas bysthållare?	BH		Concept
Förkortningar	1000	Vilken morgontidning förkortas DN?	Dagens Nyheter		Organization	Other
Förkortningar	2000	Vilket flygbolag heter egentligen Scandinavian Airlines System?	SAS		Organization
Förkortningar	5000	Vad står förkortningen www för?	World wide web		Concept	Other
Förkortningar	10000	Under vilken förkortning känner vi möbelföretaget Ingvar Kamprad Elmtaryd Agunnaryd?	IKEA		Organization
Bakverk	250	Vad brukar man ha för speciella bullar till Lucia?	Lussekatter	(lussebullar)	Concept
Bakverk	500	Vilken bulle fylld med mandelmassa och grädde äts främst i början av året?	Semlan	(eller fastlagsbullen)	Concept
Bakverk	1000	Vilken sorts sås serveras klassiskt till äppelkaka eller rabarberpaj?	Vaniljsås		Concept
Bakverk	2000	Vilken färg har marsipanöverdraget på en klassisk prinsesstårta?	Grönt		Concept
Bakverk	5000	Vilken blomma, gjord av marsipan, är den klassiska tårtdekorationen?	En ros		Concept
Bakverk	10000	Vilket platt bakverk med vaniljkräm eller sylt ovanpå, har namn efter Österrikes huvudstad?	Wienerbröd		Concept
Kakor	250	Vilken sorts kakor bakar man inför julen och stansar ut t ex gubbar eller grisar?	Pepparkakor		Concept
Kakor	500	Vilket djurnamn har en mjuk kaka med både mörk chokladsmet och ljus sockerkakssmet i?	Tigerkaka		Concept
Kakor	1000	Vad kallas mjuka kakor i pappersformar, ofta av sockerkakssmet och med smak av t ex blåbär eller choklad?	Muffins		Concept
Kakor	2000	Hur många sorters kakor skulle det vara på den klassiska kaffebjudningen förr?	Sju	(plus bullar och tårta!)	Amount
Kakor	5000	Vad är egentligen de små hackade bitarna i knäck?	Mandel		Concept
Kakor	10000	Vilken sorts småkakor låter som en nattlig upplevelse?	Drömmar		Concept
Dinosaurier och annat som inte finns mer	250	Vad hette den största landlevande köttätande dinosaurien?	Tyrannosaurus Rex		Concept
Dinosaurier och annat som inte finns mer	500	Vad hette det utdöda djur som såg ut som en stor hårig elefant?	Mammut		Concept
Dinosaurier och annat som inte finns mer	1000	De allra största dinosaurierna var växtätare. Vad kallas det den som inte äter kött?	Vegetarian		on
Dinosaurier och annat som inte finns mer	2000	Vilken nu levande varelse är mest lik neanderthalarna?	Människan	Neanderthalaren var en människoart erson	Other
Dinosaurier och annat som inte finns mer	5000	Vilket djur fanns förr som sabeltandat, och kan ses i t ex filmen Ice Age?	Tiger	(sabeltandad tiger)	pt
Dinosaurier och annat som inte finns mer	10000	Vad var den utdöda dronten för sorts djur?	En fågel		Concept
Skolämnen	250	I vilket ämne kan du sy kläder eller virka en grytlapp?	Textilslöjd	(syslöjd)	Concept	Other
Skolämnen	500	I vilket ämne kan du få hoppa bok eller spela boll?	Idrott	(gymnastik, gympa)	Concept	Other
Skolämnen	1000	I vilket ämne är det bra att kunna läsa noter?	Musik		Concept
Skolämnen	2000	Vilket är det första främmande språk de flesta svenska elever läser?	Engelska		Other	Other
Skolämnen	5000	Vilket ämne gäller om du löser ekvationer och dividerar?	Matte	(matematik)	Concept	Other
Skolämnen	10000	Vilket ämne undervisar läraren i som talar om ordklasser och satsdelar?	Svenska		Other	Other
I skolan	250	Vad heter skolans två terminer?	Hösttermin och vårtermin		Concept
I skolan	500	I vilken klass börjar man i mellanstadiet?	I fyran		Concept
I skolan	1000	Vad kallas den lediga tid som man har mellan lektioner?	Rast		Concept
I skolan	2000	Vad kallas det arbete man får med sig från skolan att göra hemma?	Läxor		Concept
I skolan	5000	Vem är chef över lärarna på en skola?	Rektorn		Person
I skolan	10000	Vad kallas med ett gemensamt namn de tabeller som visar vad olika tal gånger andra tal blir?	Multiplikationstabellen		Concept
Rotfrukter	250	Vilken rotfrukt är orange och serveras ofta riven?	Morot		Concept
Rotfrukter	500	Vilken är Sveriges mest ätna rotfrukt?	Potatis		Concept
Rotfrukter	1000	Vid vilken helg brukar man av tradition äta nypotatis?	Midsommar		Concept
Rotfrukter	2000	Vilken röd rotfrukt serveras ofta inlagd och skivad till pyttipanna?	Rödbeta		Concept
Rotfrukter	5000	Vilken stark liten rödvit rotfrukt serveras ofta till midsommar?	Rädisa		Concept
Rotfrukter	10000	Vilken stor rotfrukt är huvudingrediensen i rotmos?	Kålrot		Concept
Grönsaker	250	Vilka små gröna runda grönsaker växer i skidor och köps ofta djupfrysta?	Ärtor		Concept
Grönsaker	500	Vad kallas den bladgrönsak som finns som t ex isbergs- eller ruccola?	Sallad		Concept
Grönsaker	1000	Vilken grönsak består pizzasallad huvudsakligen av?	Vitkål		Concept
Grönsaker	2000	Vad kallas den långa smala lökväxt som är vit nedtill och ofta skivas i grytor och soppor?	Purjolök		Concept
Grönsaker	5000	Vad heter den bladväxt som stuvas och serveras till bl a stekt fisk?	Spenat		Concept
Grönsaker	10000	Den gurkliknande squashen kan också kallas ett namn som låter italienskt. Vilket?	Zucchini		Concept
Grönsaker som inte är gröna	250	Vilken färg har de ärtor man vanligen kokar ärtsoppa på?	Gula		Concept
Grönsaker som inte är gröna	500	Vilken vitgul grönsak har ordet "blom" i namnet?	Blomkål		Concept
Grönsaker som inte är gröna	1000	Vilken grönsak består av små gula korn som växer på kolvar?	Majs		Concept
Grönsaker som inte är gröna	2000	Vilken färg har de bönor som tillagas med sirap och ättika och äts med stekt fläsk?	Bruna		Concept
Grönsaker som inte är gröna	5000	Vilken grönsak med stark doft används till att smaksätta maträtter med och luktar "lång väg" när man ätit den?	Vitlök      Concept
Grönsaker som inte är gröna	10000	Vad kallas en extra stor tomat?	Bifftomat		Concept
I kryddlandet	250	Vad heter den kryddväxt som används vid kräftkok eller till färskpotatis?	Dill		Concept
I kryddlandet	500	Vad heter den gröna kryddväxten, ofta med lite krusiga blad, som börjar med per...?	Persilja		Concept
I kryddlandet	1000	På vilken lökväxt med lila blommor använder vi de gröna stråna klippta över mat?	Gräslök		Concept
I kryddlandet	2000	Vad kallas den kryddväxt som låter som en peng och smakar mint?	Mynta		Concept
I kryddlandet	5000	Nämn en av de två örtkryddor man brukar ha i klassisk svensk ärtsoppa?	Mejram eller timjan		Concept
I kryddlandet	10000	Vilken kryddväxt doftar och smakar citron och används ibland som dekoration på efterrätter?	Citronmeliss		Concept
Ord med dubbel betydelse	250	På fingret samt uppmaning att höra av sig?	Ring		Concept
Ord med dubbel betydelse	500	Handtag på köksskåp och blomma innan den slår ut?	Knopp		Concept
Ord med dubbel betydelse	1000	Något man läser i och ett träd?	Bok		Concept
Ord med dubbel betydelse	2000	Översta våningen och bris eller storm?	Vind		Concept
Ord med dubbel betydelse	5000	Stor sten eller kortsnagga?	Klippa		Concept
Ord med dubbel betydelse	10000	Fattig och det som handen sitter längst ut på?	Arm		Concept
Ord med dubbel betydelse II	250	Att lägga saker i och lövträd?	Ask		Concept
Ord med dubbel betydelse II	500	Ovanför ögonbrynen och att steka i?	Panna		Concept
Ord med dubbel betydelse II	1000	Delar med kniv och rosa färg?	Skär		Concept
Ord med dubbel betydelse II	2000	Lövträd och kastas på darttavla?	Pil		Concept
Ord med dubbel betydelse II	5000	Måste man betala på sin lön och piratrikedom?	Skatt		Concept	Other
Ord med dubbel betydelse II	10000	Dela bröd och CD?	Skiva		Concept	Other
Pojknamn med betydelse	250	Han kanske brummar och går i ide?	Björn		Concept
Pojknamn med betydelse	500	Han är motsatsen till natt?	Dag		Person
Pojknamn med betydelse	1000	Han har samma namn som armens översta del?	Axel		Person
Pojknamn med betydelse	2000	Han har samma namn som en skogsväg?	Stig		Person
Pojknamn med betydelse	5000	Han kan vara av flinta eller granit?	Sten		Person
Pojknamn med betydelse	10000	Han finns längst ut på trasmattans kant?	Frans		Person
Flicknamn med betydelse	250	Hon är en berättelse för små barn?	Saga		Person
Flicknamn med betydelse	500	Hon har ett namn som är en vårmånad?	Maj		Person
Flicknamn med betydelse	1000	Hon heter som att låta djupfryst ligga framme?	Tina		Person
Flicknamn med betydelse	2000	Hon har ett namn som får en att tänka på en sommarmånad?	Julia		Person
Flicknamn med betydelse	5000	Hon heter som första ordet vid starten i en löptävling?	Klara		Person
Flicknamn med betydelse	10000	Hon börjar på L och är Smålands landskapsblomma?	Linnea		Person
I TV-reklamens värld	250	För vilken värktablett gör den lille blåe mannen reklam?	Ipren		Concept
I TV-reklamens värld	500	För vilken matbutikskedja gör Stig och hans medarbetare reklam?	ICA		Organization	Other
I TV-reklamens värld	1000	Vilken snabbmatskedja utbrister I'm lovin it i sin reklam?	McDonalds		Organization
I TV-reklamens värld	2000	Vilket kaffe ska man bjuda på om man får oväntat besök?	Gevalia		Organization
I TV-reklamens värld	5000	Vad heter den tecknade hästen som göra att du kan vinna på hästar, enligt reklamen?	Harry Boy		Other
I TV-reklamens värld	10000	Vad kallas en kort filmsnutt som gör reklam för kommande program med ett engelskt namn?	Trailer		Concept	Other
På håret	250	Vilken färg har Pippi Långstrump på håret?	Det är rött		Description
På håret	500	När man samlar håret i en tofs i nacken heter det som något som finns på ett djur. Vad?	Hästsvans		Concept
På håret	1000	Vad kallas ett helt löshår med ett annat namn?	Peruk		Concept
På håret	2000	Vilken frisyr brukar Obelix alltid ha håret i?	Flätor		Concept
På håret	5000	Vad kallas det när man går till frisören och med hjälp av en kemisk vätska får lockar i håret som sitter kvar?	Permanent		Concept
På håret	10000	Vad kallas den frisyr med rakad skalle och en krans med hår som munkar traditionellt bär?	Tonsur		Concept
I kyrkan	250	Vad kallas det när ett barn får sitt namn i kyrkan?	Dop	(barnet döps)	Concept
I kyrkan	500	När används ibland orden "i nöd och lust" i kyrkan?	Vid bröllop	(när man gifter sig)	Concept
I kyrkan	1000	Vad kallas den utbildning som många tonåringar går igenom och som avslutas högtidligt i kyrkan?	Konfirmation		Concept
I kyrkan	2000	Vad kallas den mycket tidiga mässa som hålls på juldagens morgon?	Julotta		Definition
I kyrkan	5000	Vilken uppgift i kyrkan har en kantor?	Att spela	(oftast på orgeln)	Description
I kyrkan	10000	Vad kallas det när man serverar vin och oblat i kyrkan?	Nattvard		Concept
Semester	250	Under vilken årstid tar de flesta svenskar ut sin mesta semester?	På sommaren		Description
Semester	500	Vilket färdsätt använder de flesta svenskar som semestrar i Thailand för att ta sig dit?	Flyg		Concept
Semester	1000	Vad kallas de platser där man mot avgift kan ställa upp sitt tält eller sin husvagn tillsammans med andra?	Campingplats		Concept
Semester	2000	Vad kallas resor köpta på en resebyrå där resa, hotell och ibland mat ingår?	Charterresa		Concept
Semester	5000	Vad kallas den billigare form av hotell där man ofta får städa rummet själv?	Vandrarhem		Concept
Semester	10000	Hur många veckors semester har man enligt lag i Sverige?	Fem veckor		Duration
