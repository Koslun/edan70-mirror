package corpusParser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class CorpusCreator {
    public static final String[] DATA_PATHS = new String[]{"inputData/djur och naturv2.txt",
            "inputData/gott och blandatv2.txt", "inputData/langt borta och narav2.txt",
            "inputData/musik och underhallningv2.txt", "inputData/sport och fritidv2.txt",
            "inputData/teknik och uppfinnigarv2.txt", "inputData/bocker och filmerv2.txt"};
    public ArrayList<String> trainQuestions, trainAnswerClassifications, trainAnswers, testQuestions, testAnswerClassifications, testAnswers;
    private DataParser dataParser;


    public static void main(String[] args) {
        CorpusCreator corpusCreator = new CorpusCreator();
        corpusCreator.createCorpus();

        try {
            corpusCreator.writeToFile("train", corpusCreator.trainQuestions, corpusCreator.trainAnswerClassifications);
        } catch (FileNotFoundException e) {
            System.err.println("Could not create training corpus.");
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            corpusCreator.writeToFile("test", corpusCreator.testQuestions, corpusCreator.testAnswerClassifications);
        } catch (FileNotFoundException e) {
            System.err.println("Could not create testing corpus.");
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public CorpusCreator(){
        trainQuestions = new ArrayList<String>();
        trainAnswerClassifications = new ArrayList<String>();
        trainAnswers = new ArrayList<String>();

        testQuestions = new ArrayList<String>();
        testAnswerClassifications = new ArrayList<String>();
        testAnswers = new ArrayList<String>();

        this.dataParser = new DataParser();
    }

    public void createCorpus(){
        for (int i = 0; i < DATA_PATHS.length; i++) {
            try {
                dataParser.readFile(DATA_PATHS[i]);
            } catch (IOException e) {
                System.out.println("IOException: The file " + DATA_PATHS[i] + " was not found!");
                e.printStackTrace();
            }
        }
        System.out.println("Finished Parsing the corpus");

        splitIntoTrainAndTest(dataParser);

        System.out.println("Corpus created.");

        System.out.println("#Train Questions: " + trainQuestions.size());
        System.out.println("#Train Answers: " + trainAnswerClassifications.size());

        System.out.println("#Test Questions: " + testQuestions.size());
        System.out.println("#Test Answers: " + testAnswerClassifications.size());
    }

    private void splitIntoTrainAndTest(DataParser dataParser){
        for(int i = 0; i < dataParser.getAnswerClassifications().size(); i++){
            String question = dataParser.getQuestions().get(i);
            String answerClassification = dataParser.getAnswerClassifications().get(i);
            String answer = dataParser.getAnswers().get(i);

            if(i % 10 == 0) { // Splitting into 10% test and 90% train

                testQuestions.add(question);
                testAnswerClassifications.add(answerClassification);
                testAnswers.add(answer);
            } else {
                trainQuestions.add(question);
                trainAnswerClassifications.add(answerClassification);
                trainAnswers.add(answer);
            }
        }
    }

    public void writeToFile(String filePath, ArrayList<String> questions, ArrayList<String> answerClassifications)
            throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter(filePath + "_corpus.txt", "UTF-8");
        for (int i = 0; i < answerClassifications.size(); i++) {
            writer.print(answerClassifications.get(i) + "\t");
            writer.print(questions.get(i));
            writer.println();
        }
        writer.close();
    }
}
