package corpusParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class DataParser
{
  private String filePath;
  private ArrayList<String> questions;
  private ArrayList<String> answers;
  private ArrayList<String> answerClassifications;
  
  public DataParser()
  {
    this.questions = new ArrayList<String>();
    this.answerClassifications = new ArrayList<String>();
    answers = new ArrayList<String>();
  }
  
  public void readFile(String filePath)
    throws IOException
  {
    this.filePath = filePath;
    BufferedReader br = new BufferedReader(new FileReader(this.filePath));
    
    System.out.println("Parsing " + this.filePath + "...");

    while (br.ready())
    {
      String line = br.readLine();

      line = line.trim();
      int i = 0;
      char c = line.charAt(i);
      while ((c < '0') || (c > '9'))
      {
        i++;
        c = line.charAt(i);
      }
      i++;
      while ((c >= '0') && (c <= '9'))
      {
        i++;
        c = line.charAt(i);
      }
      i++;
      String question = "";
      while (line.charAt(i) != '?')
      {
        question = question + line.charAt(i);
        i++;
      }
      question = question.toLowerCase();
      this.questions.add(question);

      int questionEndIndex = i;
      
      String answerClassification = "";
      i = line.length() - 1;
      while (Character.isLetter(line.charAt(i)))
      {
        answerClassification = answerClassification + line.charAt(i);
        i--;
      }
      int answerClassificationEndIndex = i;
      answerClassification = new StringBuilder(answerClassification).reverse().toString();
      answerClassification = answerClassification.toLowerCase();
      this.answerClassifications.add(answerClassification);

      String answer = line.substring(questionEndIndex + 2, answerClassificationEndIndex);
      answer = answer.trim().toLowerCase();
      answers.add(answer);
    }

    System.out.println(this.questions.size() + " lines parsed.");
  }

  /**
   * Returns all the questions from the corpus
   * @return questions as ArrayList
   */
  public ArrayList<String> getQuestions(){
	  return questions;
  }

  /**
   * Returns all the answerClassifications from the corpus
   * @return answerClassifications as ArrayList
   */
  public ArrayList<String> getAnswerClassifications(){
	  return answerClassifications;
  }

  public ArrayList<String> getAnswers() {
    return answers;
  }
}
