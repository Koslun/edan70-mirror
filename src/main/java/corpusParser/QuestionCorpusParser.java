package corpusParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class QuestionCorpusParser {
	
	final String BOCKER_FILMER = "corpus/bocker och filmerv2.txt";
	final String DJUR_NATUR = "corpus/djur och naturv2.txt";
	final String GOTT_OCH_BLANDAT = "corpus/gott och blandatv2.txt";
	final String LANGT_BORT_OCH_NARA = "corpus/langt borta och narav2.txt";
	final String MUSIK_OCH_UNDERHALLNING = "corpus/musik och underhallningv2.txt";
	final String SPORT_OCH_FRITID = "corpus/sport och fritidv2.txt";
	final String TEKNIK_OCH_UPPFINNINGAR = "corpus/teknik och uppfinnigarv2.txt";

	private DataParser dataParser;
	private ArrayList<String> questionFiles, questions, answerClassifications;
	
	public QuestionCorpusParser(){
		questionFiles = new ArrayList<String>(Arrays.asList(BOCKER_FILMER, DJUR_NATUR, GOTT_OCH_BLANDAT, LANGT_BORT_OCH_NARA, MUSIK_OCH_UNDERHALLNING, SPORT_OCH_FRITID, TEKNIK_OCH_UPPFINNINGAR));
		questions = new ArrayList<String>();
		answerClassifications = new ArrayList<String>();
		loopFiles();
	}
	
	private void loopFiles(){
		dataParser = new DataParser();
		for(String fileName: questionFiles){
			try {
				dataParser.readFile(fileName);
				questions.addAll(dataParser.getQuestions());
				answerClassifications.addAll(dataParser.getAnswerClassifications());
				
			} catch (IOException e) {
				System.out.println("Couldn't find the file QuestionCorpusParser was looking for");
				e.printStackTrace();
			}
		}		
	}
	
	/**
	 * Returns all the questions
	 * @return
	 */
	public ArrayList<String> getAllQuestions(){
		return questions;
	}
	
	/**
	 * Returns all the classifications
	 * @return
	 */
	public  ArrayList<String> getAllClassifications(){
		return answerClassifications;
	}
	
}
