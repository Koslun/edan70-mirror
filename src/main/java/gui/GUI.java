package gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.Toolkit;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class GUI {
	private JFrame frame;
	private JPanel contentBorad, upperScreen, lowerScreen;
	private BoxLayout layout, upperLayout;
	private BorderLayout lowerLayout;
	private TextArea ta;
	private TextField tf;
	private SearchButton searchButton;
	
	
	public GUI(){
		
		ta = new TextArea();
		tf = new TextField("write question here");
		searchButton = new SearchButton(ta, tf);
		
		//The foundation of the GUI
		frame = new JFrame("Question/Answer system");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1000, 700);
		contentBorad = new JPanel();
		layout = new BoxLayout(contentBorad, BoxLayout.Y_AXIS);
		contentBorad.setBackground(Color.DARK_GRAY);
		contentBorad.setLayout(layout);
		contentBorad.setBorder(new EmptyBorder(10, 10, 10, 10)); // (up, left, down, right)
		frame.add(contentBorad);
		
		// Upper part of the screen
		upperScreen = new JPanel();
		upperLayout = new BoxLayout(upperScreen, BoxLayout.X_AXIS);
		upperScreen.setLayout(upperLayout);
		upperScreen.setBorder(new EmptyBorder(10, 10, 10, 10));
		upperScreen.add(tf);
		upperScreen.add(searchButton);
		contentBorad.add(upperScreen);
		
		// Lower part of the screen
		lowerScreen = new JPanel();
		lowerLayout = new BorderLayout();
		lowerScreen.setLayout(lowerLayout);
		lowerScreen.setBorder(new EmptyBorder(10, 10, 10, 10));
		lowerScreen.add(ta, BorderLayout.CENTER);
		contentBorad.add(lowerScreen);

		
		frame.setResizable(false);
		frame.setVisible(true);
	}
	
	
	public void updateTextContent(String text){
		searchButton.updateText(text);
	}
	
	public String getQuestion(){
		return tf.getText();
	}
	
	
}
