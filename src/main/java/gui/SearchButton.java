package gui;

import java.awt.Button;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import queryPipeline.AnswerCandidate;
import queryPipeline.QueryExecutor;
import queryPipeline.QueryResult;


public class SearchButton extends Button implements ActionListener{
	
	private TextArea ta;
	private TextField tf;
	private String allText = "";
	private QueryExecutor qe;
	
	public SearchButton(TextArea ta, TextField tf){
		this.ta = ta;
		this.tf = tf;
		setLabel("Search");
		addActionListener(this);
		qe = new QueryExecutor();
	}
	
	
	public void updateText(String text){
		allText += text + "\n";
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String question = tf.getText();
		QueryResult queryResult = qe.executeQuery(question);
		ArrayList<AnswerCandidate> result = queryResult.getRerankedRanking();
		ArrayList<AnswerCandidate> baseLineResults = queryResult.getLuceneRanking();
		String topResults = "";

		int topResultsSize = 200;
		if(result.size() < topResultsSize){
			topResultsSize = result.size();
		}

		for(int i = 0; i < topResultsSize; i++){
			AnswerCandidate reRankedCandidate = result.get(i);
			topResults += reRankedCandidate.getLemma() + "\t" + reRankedCandidate.getScore() + "\t" + reRankedCandidate.getClassification();
			topResults += "\t|\t";
			AnswerCandidate baselineCandidate = baseLineResults.get(i);
			topResults += baselineCandidate.getLemma() + "\t" + baselineCandidate.getScore() + "\t" + baselineCandidate.getClassification();
			topResults += "\n";
		}
		String titleRow  = "Reranked results \t\t|\t Baseline results\n";
		titleRow 		+= "================ \t\t|\t ================\n";
		ta.setText(titleRow + topResults);
	}
}
