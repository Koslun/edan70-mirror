package queryPipeline;

public class AnswerCandidate implements Comparable{
	private String word, classification, lemma;
	private float score;
	private int frequency;
	private boolean failedToClassify;
	
	
	public AnswerCandidate(String word, String lemma, String classification, float score) {
		this.word = word;
		this.lemma = lemma;
		this.classification = classification;
		this.score = score;
		frequency = 0;
		if(classification.equals("-")){
			failedToClassify = true;
		} else {
			failedToClassify = false;
		}
	}

	public boolean failedtoClassify(){
		return failedToClassify;
	}
	
	//public String getWord(){
	//	return word;
	//}
	public String getLemma(){
		return lemma;
	}

	public AnswerCandidate clone(){
		AnswerCandidate myClone = new AnswerCandidate(word, lemma, classification, score);
		myClone.frequency = this.frequency;
		return myClone;
	}

	public int getFrequency(){
		return frequency;
	}

	public float getScore(){
		return score;
	}

	public void addScore(float score){
		this.score += score;
		frequency++;
	}

	public void applyClassificationWeight(float weight){
		score = score * weight;
	}
	
	public String getClassification(){
		return classification;
	}

	public boolean hasClassification(String classification){
		return this.classification.equals(classification);
	}

	@Override
	public int hashCode() {
	//	final int prime = 31;
	//	int result = 1;
	//	result = prime * result
	//			+ ((classification == null) ? 0 : classification.hashCode());
	//	result = prime * result + ((word == null) ? 0 : word.hashCode());
	//	return result;
		return this.lemma.toLowerCase().hashCode();
	}
	
	public String getKeysInString() {
		return word + " " + classification;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnswerCandidate other = (AnswerCandidate) obj;
	/*	if (classification == null) {
			if (other.classification != null)
				return false;
		} else if (!classification.equals(other.classification))
			return false;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;*/
		if (lemma == null) {
			System.out.println("lemma is null");
			if (other.lemma != null)
				return false;
		} else if (!lemma.trim().equalsIgnoreCase(other.lemma.trim())){
		//	System.out.println(lemma + "=/=" + other.lemma);
			return false;
		}
		//System.out.println(lemma + "=" + other.lemma);
		return true;
	}

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		AnswerCandidate other = (AnswerCandidate) arg0;
		if(this.score < other.score){
			return 1;
		} else if(this.score > other.score){
			return -1;
		}
		return 0;
	}
}
