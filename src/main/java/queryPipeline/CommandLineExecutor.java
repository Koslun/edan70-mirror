package queryPipeline;


import java.io.*;

public class CommandLineExecutor {
	
	public CommandLineExecutor(){}

      public void executeCommand(String command) {

           try {
               Runtime rt = Runtime.getRuntime();
               Process pr = rt.exec(command);

           //    BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));

           //    String line = null;

           //    while((line = input.readLine()) != null) {
               //    System.out.println(line);
           //    }

               int exitVal = pr.waitFor();
               if(exitVal == 0) {
            	   System.out.println("All OK!");
               } else {
            	   System.out.println("External programs were not executed correctly. Please check for solutions.");
               }

           } catch(Exception e) {
               System.out.println(e.toString());
               e.printStackTrace();
           }
       }
}
