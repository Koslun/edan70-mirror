package queryPipeline;


import corpusParser.CorpusCreator;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;


public class QASystemTest{

	public static void main(String[] args) {
		QASystemTest qaSystemTest = new QASystemTest();
		System.out.println("Testing with the test corpus.");
		Date start = new Date();

		CorpusCreator corpusCreator = new CorpusCreator();
		corpusCreator.createCorpus();

		ArrayList<QueryResult> queryResults = qaSystemTest.testQuestions(corpusCreator.testQuestions, corpusCreator.testAnswers,
				corpusCreator.testAnswerClassifications);

		Date end = new Date();
		System.err.println("Test suite completed.");
		System.err.println("The test took: " + (end.getTime() - start.getTime())/1000F + " seconds");

		try {
			qaSystemTest.writeToCSVFile("test_results", queryResults);
		} catch (FileNotFoundException e) {
			System.err.println("Could not find file for writing to CSV");
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		qaSystemTest.evaluateBaseline();
		qaSystemTest.evaluateReranked();
	}

	private QueryExecutor queryExecutor;
	private ArrayList<QueryResult> queryResults;
	private ArrayList<Float> baselineRanks, rerankingRanks;
	private int failedBaseline, failedReranks;

	public void evaluateBaseline(){
		System.err.println("\n_________________________________________________________");
		System.err.println("Results with baseline ranking");
		evaluateRanks(baselineRanks, "BaseLineResults", failedBaseline);
		System.err.println("---------------------------------------------------------");
	}

	public void evaluateReranked(){
		System.err.println("______________________________________________________");
		System.err.println("Results after reranking");
		evaluateRanks(rerankingRanks, "RerankingResults", failedReranks);
		System.err.println("------------------------------------------------------");
	}

	private void evaluateRanks(ArrayList<Float> ranks, String outputFile, int failedToFind){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(outputFile + ".csv", "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		StringBuffer result = new StringBuffer("");
		result.append("MRR: " + calculateMRR(ranks) + "\n");
		result.append("Mean: " + calculateMean(ranks) + "\n");
		result.append("Median: " + calculateMedian(ranks) + "\n");
		result.append("Found answers for "+ ranks.size() + " questions." + "\n");
		result.append("Did not find answers for " + failedToFind + " questions." + "\n");

		String resultStr = result.toString();
		System.err.print(resultStr);
		writer.print(resultStr);

		writer.close();
	}

	public QASystemTest(){
		queryExecutor = new QueryExecutor();
		baselineRanks = new ArrayList<Float>();
		rerankingRanks = new ArrayList<Float>();
		queryResults = new ArrayList<QueryResult>();
		failedBaseline = 0;
		failedReranks = 0;
	}

	private ArrayList<QueryResult> testQuestions(ArrayList<String> questions, ArrayList<String> answers, ArrayList<String> answerClassifications){
		for(int i = 0; i < questions.size()/2; i++){
			QueryResult queryResult = testQuestion(questions.get(i), answers.get(i), answerClassifications.get(i));
			queryResults.add(queryResult);
		}

		return queryResults;
	}

	/**
	 * Does a passage retrieval of a question and then reranks the results by predicted the answer type. Returns the
	 * query result object containing all the information related to the query.
	 * @param question
	 * @param answer
	 * @param answerClassification
	 * @return Query Result
	 */
	private QueryResult testQuestion (String question, String answer, String answerClassification){
		Date start = new Date();
		answer = answer.toLowerCase();
		QueryResult queryResult = queryExecutor.executeQuery(question);

		queryResult.setQuestion(question);
		queryResult.setCorrectAnswer(answer);
		queryResult.setCorrectAnswerClassification(answerClassification);
		queryResult.calculateRank(answer);

		queryResult.setExecutionTime(start);

		Float baselineRank = queryResult.getBaselineRank();
		if(baselineRank > 0f){
			baselineRanks.add(baselineRank);
		} else {
			failedBaseline++;
		}

		Float rerankingRank = queryResult.getRerankedRank();
		if(rerankingRank > 0f){
			rerankingRanks.add(rerankingRank);
		} else {
			failedReranks++;
		}

		return queryResult;
	}

	private static Float calculateMRR(ArrayList<Float> ranks){
		Float sumOfReciprocalRanks = 0f;
		for(Float rank : ranks){
			if(rank > 0f){
				sumOfReciprocalRanks += 1/rank;
			}
		}
		Float nbrOfQueries = ((float) (ranks.size()));
		Float meanReciprocalRank = sumOfReciprocalRanks / nbrOfQueries;
		System.out.println("Sum of reciprocal ranks:" + sumOfReciprocalRanks);
		System.out.println("Nbr of queries: " + nbrOfQueries);
		return meanReciprocalRank;
	}

	private static Float calculateMean(ArrayList<Float> ranks){
		Float rankSum = 0f;
		for(Float rank: ranks){
			if(rank > 0f){
				rankSum += rank;
			}
		}
		Float mean = rankSum / ((float) (ranks.size()));
		return mean;
	}

	private static Float calculateMedian(ArrayList<Float> ranks){
		ArrayList<Float> ranksCopy = new ArrayList<Float>(ranks);
		Collections.sort(ranksCopy);
		Float median = 0f;

		if(ranks.size() > 0f){
			median = ranksCopy.get((ranksCopy.size())/2);
		}
		return median;
	}

	public void writeToCSVFile(String outputFile, ArrayList<QueryResult> queryResults)
			throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter writer = new PrintWriter(outputFile + ".csv", "UTF-8");

		writer.print(QueryResult.getCSVHeaders());

		for (int i = 0; i < queryResults.size(); i++) {
			writer.print(queryResults.get(i).toCSVRow());
		}

		writer.close();
	}

}
