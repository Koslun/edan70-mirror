package queryPipeline;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.lucene.queryparser.classic.ParseException;

import se.su.ling.stagger.FormatException;
import se.su.ling.stagger.TagNameException;


public class QueryExecutor {
    private static final String DEFAULT_INDEX_PATH = "../wiki_index";
    private static final String DEFAULT_DATA_PATH = "";
    private StaggerExecutor se;

    public QueryExecutor() {
        try {
            se = new StaggerExecutor();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public QueryResult executeQuery(String query) {
        String indexPath, dataPath;
        indexPath = DEFAULT_INDEX_PATH;
        dataPath = DEFAULT_DATA_PATH;

        //1. Classification.
        QuestionAnswerClassifier q = new QuestionAnswerClassifier(query);
        String classification = "NO_CLASSIFICATION";
        try {
            classification = q.predict();
        } catch (IOException e2) {
            System.out.println("QuestionClassifier could not predict a classification. Please check for solutions.");
            e2.printStackTrace();
        }

        System.out.println("Predicted classification for the question was: " + classification);

        //2. Search for articles using Lucene.

        WikiSearcher wikiSearcher = null;
        try {
            wikiSearcher = new WikiSearcher(indexPath);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("No index at " + indexPath);
        }

        HashMap<String, Float> articleScores = new HashMap<String, Float>();

        try {
            articleScores = wikiSearcher.searchAndRecord(query, 50);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //3. Stagger...

        System.out.println("Starting to stagger");
        Date start = new Date();
        boolean staggerCrashed = false;
        WordCounter wc = null;
        try {
            wc = se.stag(articleScores, dataPath);

            if (classification.equals("location")) {
                classification = "place";
            }
/*
            HashMap<AnswerCandidate, Float> candidates = wc.selectCandidates(classification);


            for (Entry<AnswerCandidate, Float> e : candidates.entrySet()) {
                AnswerCandidate answerCandidate = e.getKey();
                answerCandidate.score = e.getValue();
                answerList.add(answerCandidate);
				System.out.println(e.getKey().getKeysInString() + "\t" + e.getValue());
            }
            Collections.sort(answerList);
            for (AnswerCandidate answerCandidate : answerList) {
               	System.out.println(answerCandidate.getWord() + " - " + answerCandidate.score );
            }*/

        } catch (FormatException e) {
            staggerCrashed = true;
            System.err.print("StaggerFormatException");
            e.printStackTrace();
        } catch (TagNameException e) {
            staggerCrashed = true;
            System.err.print("StaggerTagNameException");
            e.printStackTrace();
        } catch (IOException e) {
            staggerCrashed = true;
            System.err.print("StaggerIOException");
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            staggerCrashed = true;
            System.err.print("StaggerArrayIndexOutOfBoundsException");
            e.printStackTrace();
        }

        Date end = new Date();
        if (staggerCrashed) {
            System.err.println(". Staggering failed, probably skipping article candidate for query: " + query);
        } else {
            System.err.println("Finished Staggering.");
        }

        System.err.println("Staggering took: " + (end.getTime() - start.getTime()) / 1000F
                + " seconds");


        ArrayList<AnswerCandidate> baselineRanking, rerankedRanking;
        if(wc != null){
            baselineRanking = wc.baseLineSortAnswerCandidates();
            rerankedRanking = wc.reRankCandidates(classification, 0.74f);

        } else {
            baselineRanking = new ArrayList<AnswerCandidate>();
            rerankedRanking = new ArrayList<AnswerCandidate>();
        }
        QueryResult queryResult = new QueryResult(baselineRanking, rerankedRanking, classification, staggerCrashed, se);

        return queryResult;
    }
}
