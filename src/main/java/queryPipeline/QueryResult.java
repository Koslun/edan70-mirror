package queryPipeline;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by wamai on 19/01/15.
 */
public class QueryResult {
    public static final String S = ";";
    private ArrayList<AnswerCandidate> luceneRanking, rerankedRanking;
    private String correctAnswerFoundRR, correctAnswerFoundBR, correctAnswer, predictedAnswerClassification, correctAnswerClassification, question;
    private Float baselineRank, rerankedRank, executionTimeInSecs;
    private boolean staggerCrashed;
    private boolean noAnswerFoundBR, noAnswerFoundRR;
    private int staggerSuccesses, staggerFailures;
    private StaggerExecutor staggerExecutor;

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Float getBaselineRank(){
        return baselineRank;
    }

    public Float getRerankedRank(){
        return rerankedRank;
    }

    public void setCorrectAnswerClassification(String correctAnswerClassification){
        this.correctAnswerClassification = correctAnswerClassification;
    }

    public void setQuestion(String question){
        this.question = question;
        if(staggerCrashed){
            System.err.println("Skipped counting question: \"" + question + "\" due to a stagger error.");
        }
    }

    public QueryResult(ArrayList<AnswerCandidate> luceneRanking, ArrayList<AnswerCandidate> rerankedRanking, String predictedAnswerClassification,
                       boolean staggerCrashed, StaggerExecutor staggerExecutor) {
        this.luceneRanking = luceneRanking;
        this.rerankedRanking = rerankedRanking;
        this.predictedAnswerClassification = predictedAnswerClassification;
        this.staggerCrashed = staggerCrashed;

        correctAnswerFoundBR = "<N/A>";
        correctAnswerFoundRR = "<N/A>";
        correctAnswer = "<N/A>";
        executionTimeInSecs = -1F;
        noAnswerFoundBR = false;
        noAnswerFoundRR = false;
        staggerSuccesses = 0;
        staggerFailures = 0;
        this.staggerExecutor = staggerExecutor;
    }

    private void calculateStaggerSuccess(){
        for(AnswerCandidate answerCandidate: luceneRanking){
            if(answerCandidate.failedtoClassify()){
                staggerFailures++;
            } else {
                staggerSuccesses++;
            }
        }
    }

    public void setExecutionTime(Date start){
        Date end = new Date();
        executionTimeInSecs = (end.getTime() - start.getTime())/1000F;
        System.out.println("Baseline Rank: " + baselineRank + ". RerankedRank: " + rerankedRank + ". Took: " +
                executionTimeInSecs + " seconds");
    }

    public void calculateRank(String correctAnswer){
        ArrayList<AnswerCandidate> answerWords = staggerExecutor.documentStringToCandidates(correctAnswer, 0f);

        baselineRank = getRankBR(luceneRanking, answerWords, correctAnswer);
        rerankedRank = getRankRR(rerankedRanking, answerWords, correctAnswer);
        calculateStaggerSuccess();
    }



    private Float getRankBR(ArrayList<AnswerCandidate> answerCandidates, ArrayList<AnswerCandidate> answerWords, String answer){
        Float rank = 1f;
        for(AnswerCandidate answerCandidate : answerCandidates){
            String foundAnswer = answerCandidate.getLemma().toLowerCase();
            if(answerWords.contains(answerCandidate) || answer.equals(foundAnswer)) {
                correctAnswerFoundBR = foundAnswer;
                return rank;
            }
            rank = rank + 1f;
        }
        noAnswerFoundBR = true;
        return -1f;
    }

    private Float getRankRR(ArrayList<AnswerCandidate> answerCandidates, ArrayList<AnswerCandidate> answerWords, String answer){
        Float rank = 1f;
        for(AnswerCandidate answerCandidate : answerCandidates){
            String foundAnswer = answerCandidate.getLemma().toLowerCase();
            if(answerWords.contains(answerCandidate)  || answer.equals(foundAnswer)) {
                correctAnswerFoundRR = foundAnswer;
                return rank;
            }
            rank = rank + 1f;
        }
        noAnswerFoundRR = true;
        return -1f;
    }

    public ArrayList<AnswerCandidate> getLuceneRanking() {
        return luceneRanking;
    }

    public ArrayList<AnswerCandidate> getRerankedRanking() {
        return rerankedRanking;
    }

    public static String getCSVHeaders(){
        StringBuffer headers = new StringBuffer("");

        headers.append("Question" + S);

        headers.append("Actual Answer" + S);
        headers.append("Found Answer (BR)" + S);
        headers.append("Found Answer (RR)" + S);

        headers.append("Actual Classification" + S);
        headers.append("Predicted Classification" + S);

        headers.append("Stagger successes" + S);
        headers.append("Stagger failures" + S);

        headers.append("Rank" + S);
        headers.append("Rerank" + S);

        headers.append("Exec. Time (Sec)" + S);

        headers.append("\n");
        return headers.toString();
    }

    public String toCSVRow(){
        StringBuffer CSV = new StringBuffer("");
        CSV.append(question + S);

        CSV.append(correctAnswer + S);
        CSV.append(correctAnswerFoundBR + S);
        CSV.append(correctAnswerFoundRR + S);

        CSV.append(correctAnswerClassification + S);
        CSV.append(predictedAnswerClassification + S);

        CSV.append(staggerSuccesses + S);
        CSV.append(staggerFailures + S);

        if(!staggerCrashed){
            CSV.append(baselineRank + S);
            CSV.append(rerankedRank + S);
        } else {
            CSV.append("ERROR" + S);
            CSV.append("ERROR" + S);
        }

        CSV.append(executionTimeInSecs + S);

        CSV.append("\n");

        return CSV.toString();
    }

}
