package queryPipeline;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class QuestionAnswerClassifier {
	
	private String questionFilePath = "question.txt";
	
	public QuestionAnswerClassifier(String query){
		try {
			PrintWriter pw = new PrintWriter(new File(questionFilePath));
			pw.write("concept" + "\t" + query);
		//	pw.write(query);
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println("Could not write to file: " + questionFilePath);
			e.printStackTrace();
		}
		
	}
	
	public String predict() throws IOException{
		CommandLineExecutor cle = new CommandLineExecutor();
		cle.executeCommand("python text-predict.py -f " + questionFilePath + " ans_questions.model predict_result_my_question.txt");
		
		BufferedReader br = new BufferedReader(new FileReader("predict_result_my_question.txt"));
		String line = "";
		for(int i = 0; i < 7; i++){
			line = br.readLine();
		}

		String classification = "";
		int i = 0;
		while(Character.isLetter(line.charAt(i))){
			classification += line.charAt(i);
			i++;
		}
		classification = classification.toLowerCase();
		return classification;
	}
	
}
