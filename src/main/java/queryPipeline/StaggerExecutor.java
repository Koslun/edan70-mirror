package queryPipeline;

import java.io.*;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map.Entry;

import java.util.Date;

import java.util.zip.GZIPInputStream;

import se.su.ling.stagger.*;

public class StaggerExecutor {
	String lang;
	Tagger tagger;
	TaggedToken[][] inputSents;
	boolean preserve = false, plainOutput = false;
	int nbrFilesStagged;

	/*
	public static void main(String[] args) {
//		String docsSource = "";//args[0];
		String wikiPath = "../articles";

		boolean create = true;

//		File docDir = new File(docsSource);
//		if (!docDir.exists() || !docDir.canRead()) {
//			System.out
//					.println("Document directory '"
//							+ docDir.getAbsolutePath()
//							+ "' does not exist or is not readable, please check the path");
//			System.exit(1);
//		}

		try {
//			System.out.println("Staggering from " + "from '" + docsSource + "' to directory '" + indexPath + "'...");
			System.out.println("Staggering from " + "from '" + wikiPath + "' to same directory '");

			StaggerExecutor staggerExecutor = new StaggerExecutor();
			Date start = new Date();
			StaggerExecutor.tagFile(staggerExecutor, new File(wikiPath), start);

			Date end = new Date();
			System.out.println("Staggering took: " + (end.getTime() - start.getTime())
					+ " milliseconds");

		} catch (IOException e) {
			System.out.println(" caught a " + e.getClass()
					+ "\n with message: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (TagNameException e) {
			e.printStackTrace();
		}
	}
	*/

	public StaggerExecutor() throws FileNotFoundException, IOException,
			ClassNotFoundException {
		nbrFilesStagged = 0;
		inputSents = null;
		String modelFile = "../models/swedish.bin";
		ObjectInputStream modelReader = new ObjectInputStream(
				new FileInputStream(modelFile));
		System.out.println("Loading Stagger model at \"" + modelFile + "\". This will take between 1 and 3 minutes...");
		Date start = new Date();
		tagger = (Tagger) modelReader.readObject();
		lang = "sv";// tagger.getTaggedData().getLanguage();
		modelReader.close();
		Date end = new Date();
		System.out.println("Finished loading Stagger model. With language code: " + lang);
		System.out.println("Loading Stagger model took: " + (end.getTime() - start.getTime())/1000F
				+ " seconds");
	}
/*
	/**
	 * Tags the given file with stagger using the given writer, or if a directory is
	 * given, recurses over files and directories found under the given
	 * directory.
	 *
	 * @param staggerExecutor Writer to the index where the given file/dir info will be
	 *               stored
	 * @param file   The file to index, or the directory to recurse into to find
	 *               files to index
	 * @param mainStart When the staggering was started.
	 *
	 * @throws IOException If there is a low-level I/O error
	 */
/*	static void tagFile(StaggerExecutor staggerExecutor, File file, Date mainStart) throws IOException, TagNameException {
		if (file.canRead()) {
			if (file.isDirectory()) {
				String[] files = file.list();
				if (files != null) {
					for (int i = 0; i < files.length; i++) {
						System.out.println("Tagging: " + files[i]);
						tagFile(staggerExecutor, new File(file, files[i]), mainStart);
					}
				}
			} else {
				//staggerExecutor.writeToFile(file.getPath(), mainStart);
			}
		}
	}*/
/*
	private void writeToFile(String inputFile, Date mainStart) throws IOException, TagNameException {
		nbrFilesStagged++;
		if(nbrFilesStagged > 100){
			Date end = new Date();
			System.err.println("Staggering took: " + (end.getTime() - mainStart.getTime())/1000F
					+ " seconds");
			System.exit(1);
		}
		String fileID = (new File(inputFile)).getName().split("\\.")[0];
		BufferedReader reader = openUTF8File(inputFile);
		BufferedWriter writer = null;
		String outputFile = inputFile
				+ (plainOutput ? ".plain" : ".conll");
		writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8"));

		Tokenizer tokenizer = new SwedishTokenizer(reader);//getTokenizer(reader, lang);
		ArrayList<Token> sentence;
		int sentIdx = 0;
		long base = 0;
		while ((sentence = tokenizer.readSentence()) != null) {
			TaggedToken[] sent = new TaggedToken[sentence.size()];
			if (tokenizer.sentID != null) {
				if (!fileID.equals(tokenizer.sentID)) {
					fileID = tokenizer.sentID;
					sentIdx = 0;
				}
			}
			for (int j = 0; j < sentence.size(); j++) {
				Token tok = sentence.get(j);
				String id;
				id = fileID + ":" + sentIdx + ":" + tok.offset;
				sent[j] = new TaggedToken(tok, id);
			}
			TaggedToken[] taggedSent = tagger.tagSentence(sent, true,
					false);

			tagger.getTaggedData().writeConllSentence(
			(writer == null) ? System.out : writer, taggedSent,
			plainOutput);

			sentIdx++;
		}
		tokenizer.yyclose();

		// Insert class to make cool stuff with stuff in strWriter
		if (writer != null) {
			writer.close();
		}
	}
*/
	public WordCounter stag(HashMap<String, Float> inputFiles, String dataPath)
			throws FormatException, TagNameException, IOException, ArrayIndexOutOfBoundsException {
		WordCounter wc = new WordCounter();
		boolean extendLexicon = true, hasNE = true;
		tagger.setExtendLexicon(extendLexicon);
		if (!hasNE) {
			tagger.setHasNE(false);
		}
		for (String inputFile : inputFiles.keySet()) {
            String inputFileTmp = inputFile;
			inputFile = dataPath + inputFile;

			writeToString(inputFile, inputFileTmp, wc, inputFiles);
		}
		return wc;
	}

	public WordCounter staggerArticles(HashMap<String, Float> articleFiles, String dataPath){

		boolean extendLexicon = true, hasNE = true;
		tagger.setExtendLexicon(extendLexicon);
		if (!hasNE) {
			tagger.setHasNE(false);
		}

		WordCounter wordCounter = new WordCounter();

		for (String inputFile : articleFiles.keySet()) {
			Float score = articleFiles.get(inputFile);
			inputFile = dataPath + inputFile;

			String article = readArticle(inputFile);
			ArrayList<AnswerCandidate> answerCandidates = documentStringToCandidates(article, score);
			wordCounter.addAllCandidates(answerCandidates);
		}

		return wordCounter;
	}

	private String readArticle(String inputFile){
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(inputFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		StringBuffer article = new StringBuffer("");
		try {
			while (br.ready()) {
				article.append(br.readLine() + "\n");
            }
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(article);
		return article.toString();
	}
/*
	private void writeStaggedToSystemOut(String inputFile) throws FormatException, IOException, TagNameException {
		inputSents = tagger.getTaggedData().readConll(inputFile, null,
				true, !inputFile.endsWith(".conll"));
		Evaluation eval = new Evaluation();
		int count = 0;
		BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(System.out, "UTF-8"));
		for (TaggedToken[] sent : inputSents) {
			if (count % 100 == 0) {
				System.err
						.print("Tagging sentence nr: " + count + "\r");
			}
			count++;
			TaggedToken[] taggedSent = tagger.tagSentence(sent, true,
					preserve);

			eval.evaluate(taggedSent, sent);
			tagger.getTaggedData().writeConllGold(writer, taggedSent,
					sent, plainOutput);
		}
		System.err.println("Tagging sentence nr: " + count);
		System.err.println("POS accuracy: " + eval.posAccuracy() + " ("
				+ eval.posCorrect + " / " + eval.posTotal + ")");
		System.err.println("NE precision: " + eval.nePrecision());
		System.err.println("NE recall:    " + eval.neRecall());
		System.err.println("NE F-score:   " + eval.neFscore());
	}
*/
	public ArrayList<AnswerCandidate> documentStringToCandidates(String documentString, Float score){
		ArrayList<AnswerCandidate> answerCandidates = new ArrayList<AnswerCandidate>();

		BufferedReader bufferedReader = new BufferedReader(new StringReader(documentString));

		Tokenizer tokenizer = new SwedishTokenizer(bufferedReader);
		ArrayList<Token> sentence;
		int sentIdx = 0;
		try {
			while ((sentence = tokenizer.readSentence()) != null) {
                TaggedToken[] sent = new TaggedToken[sentence.size()];

                for (int j = 0; j < sentence.size(); j++) {
                    Token tok = sentence.get(j);
                    String id;
                    id = sentIdx + ":" + tok.offset;
                    sent[j] = new TaggedToken(tok, id);
                }
				TaggedToken[] taggedSent;
				try{
					taggedSent = tagger.tagSentence(sent, true, false);
				} catch(ArrayIndexOutOfBoundsException e){
					e.printStackTrace();
					continue;
				}

                TagSet posTagSet = tagger.getTaggedData().getPosTagSet();
                //	TagSet neTagSet = tagger.getTaggedData().getNETagSet();
                TagSet neTypeTagSet = tagger.getTaggedData().getNETypeTagSet();

                for (int i = 0; i < taggedSent.length; i++) {
                    TaggedToken taggedToken = taggedSent[i];

                    String posTag;
                    //	String neTag;
                    try {
                        posTag = posTagSet.getTagName(taggedToken.posTag).split("\\|")[0];
                    } catch (TagNameException e) {
                        System.err.println("TagNameException for posTag: " + taggedToken.token.value);
                        posTag = "-";
                    }
                    if((posTag.equals("PM") || posTag.equals("NN"))){
                        String tokenValue = taggedToken.token.value;
                        if( (tokenValue.length() > 1 ||
                                tokenValue.equalsIgnoreCase("å") || tokenValue.equalsIgnoreCase("ö"))){
                            String neTypeTag;
                            String tokenLF = taggedToken.lf;
                            if(taggedToken.neTypeTag >= 0){
                                try {
                                    neTypeTag = neTypeTagSet.getTagName(taggedToken.neTypeTag);
                                } catch (TagNameException e) {
                                    System.err.println("TagNameException for neTypeTag: " + tokenValue);
                                    neTypeTag = "-";
                                    e.printStackTrace();
                                }
                            } else {
                                neTypeTag = "-";
                            }
							/*try {
								neTag = neTagSet.getTagName(taggedToken.neTag);
							} catch (TagNameException e) {
								System.err.println("TagNameException for neTag" + tokenValue);
								neTag = "-";
							}*/
                            AnswerCandidate answerCandidate = new AnswerCandidate(tokenValue, tokenLF, neTypeTag, score);
							if(answerCandidates.contains(answerCandidate)){
								int indexOfCandidate = answerCandidates.indexOf(answerCandidate);
								AnswerCandidate alreadyExistingCandidate = answerCandidates.get(indexOfCandidate);
								alreadyExistingCandidate.addScore(score);
							} else {
								answerCandidates.add(answerCandidate);
							}
                        }
                    }

                }
                sentIdx++;
            }
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			tokenizer.yyclose();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return answerCandidates;
	}

	private void writeToString(String inputFile, String inputFileTmp, WordCounter wordCounter, HashMap<String, Float> inputFiles) throws IOException, TagNameException, ArrayIndexOutOfBoundsException {
		String fileID = (new File(inputFile)).getName().split("\\.")[0];
		BufferedReader reader = openUTF8File(inputFile);
	//	StringWriter strWriter = new StringWriter();

		Tokenizer tokenizer = new SwedishTokenizer(reader);//getTokenizer(reader, lang);
		ArrayList<Token> sentence;
		int sentIdx = 0;
	//	long base = 0;
		while ((sentence = tokenizer.readSentence()) != null) {
			TaggedToken[] sent = new TaggedToken[sentence.size()];
			if (tokenizer.sentID != null) {
				if (!fileID.equals(tokenizer.sentID)) {
					fileID = tokenizer.sentID;
					sentIdx = 0;
				}
			}
			for (int j = 0; j < sentence.size(); j++) {
				Token tok = sentence.get(j);
				String id;
				id = fileID + ":" + sentIdx + ":" + tok.offset;
				sent[j] = new TaggedToken(tok, id);
			}
			TaggedToken[] taggedSent = tagger.tagSentence(sent, true, false);


		//	tagger.getTaggedData().writeConllSentence(
		//			(strWriter == null) ? System.out : strWriter,
		//			taggedSent, plainOutput);

			TagSet posTagSet = tagger.getTaggedData().getPosTagSet();
		//	TagSet neTagSet = tagger.getTaggedData().getNETagSet();
			TagSet neTypeTagSet = tagger.getTaggedData().getNETypeTagSet();

			for (int i = 0; i < taggedSent.length; i++) {
				TaggedToken taggedToken = taggedSent[i];

				String posTag;
			//	String neTag;
				try {
					posTag = posTagSet.getTagName(taggedToken.posTag).split("\\|")[0];
				} catch (TagNameException e) {
					System.err.println("TagNameException for posTag: " + taggedToken.token.value);
					posTag = "-";
				}
				if((posTag.equals("PM") || posTag.equals("NN"))){
					String tokenValue = taggedToken.token.value;
					if( (tokenValue.length() > 1 ||
							tokenValue.equalsIgnoreCase("å") || tokenValue.equalsIgnoreCase("ö"))){
						String neTypeTag;
						String tokenLF = taggedToken.lf;
						if(taggedToken.neTypeTag >= 0){
							try {
								neTypeTag = neTypeTagSet.getTagName(taggedToken.neTypeTag);
							} catch (TagNameException e) {
								System.err.println("TagNameException for neTypeTag: " + tokenValue);
								neTypeTag = "-";
								e.printStackTrace();
							}
						} else {
							neTypeTag = "-";
						}

					/*	System.out.println("tokenValue: " + tokenValue);
						System.out.println("tokenLF: " + tokenLF);
						System.out.println("posTag " + posTag);
						System.out.println("neTag " + neTag);
						System.out.println("neTypeTag: " + neTypeTag);*/
						AnswerCandidate answerCandidate = new AnswerCandidate(tokenValue, tokenLF, neTypeTag, inputFiles.get(inputFileTmp));
						wordCounter.addCandidate(answerCandidate);
					}

				}
				/*try {
					neTag = neTagSet.getTagName(taggedToken.neTag);
				} catch (TagNameException e) {
					System.err.println("TagNameException for neTag" + tokenValue);
					neTag = "-";
				}*/


			}
			sentIdx++;
		}
		tokenizer.yyclose();

	//	wordCounter.countWords(strWriter.toString(), inputFiles.get(inputFileTmp));
		//wordCounter.countStuff(strWriter.toString(), inputFiles.get(inputFileTmp));
	}

	private static BufferedReader openUTF8File(String name) throws IOException {
		if (name.equals("-")) {
			return new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
		} else if (name.endsWith(".gz")) {
			return new BufferedReader(new InputStreamReader(
					new GZIPInputStream(new FileInputStream(name)), "UTF-8"));
		}
		return new BufferedReader(new InputStreamReader(new FileInputStream(
				name), "UTF-8"));
	}
/*
	private static Tokenizer getTokenizer(Reader reader, String lang) {
		Tokenizer tokenizer;
		if (lang.equals("sv")) {
			tokenizer = new SwedishTokenizer(reader);
		} else if (lang.equals("en")) {
			tokenizer = new EnglishTokenizer(reader);
		} else if (lang.equals("any")) {
			tokenizer = new LatinTokenizer(reader);
		} else {
			throw new IllegalArgumentException();
		}
		return tokenizer;
	}
*/
}
