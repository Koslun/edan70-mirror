package queryPipeline;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.util.HashMap;



public class WikiSearcher {
	private IndexSearcher indexSearcher;
	private QueryParser queryParser;
	private HashMap<String, Float> articleScores;

	public static void main(String[] args) throws IOException, ParseException {		
		String indexPath = "../wiki_index";
		//String queryStr = args[0];
		String queryStr = "vad är sveriges huvudstad";
		WikiSearcher wikiSearcher = new WikiSearcher(indexPath);
		wikiSearcher.searchAndRecord(queryStr, 200);
	}

	public WikiSearcher(String indexPath) throws IOException {
		Directory directory = FSDirectory.open(new File(indexPath));
		IndexReader indexReader = DirectoryReader.open(directory);
		indexSearcher = new IndexSearcher(indexReader);
		
		articleScores = new HashMap<String, Float>();

		Analyzer analyzer = new StandardAnalyzer();
		queryParser = new QueryParser("contents", analyzer);
	}
	
	/**
	 * Search for searchString among the files/content in the directory/file indexPath.
	 * @param searchString
	 * @throws IOException
	 * @throws ParseException
	 */
	public HashMap<String, Float> searchAndRecord(String searchString, int hitLimit) throws IOException,
			ParseException {
		articleScores.clear();
		if(articleScores.size() > 0){
			System.err.println("articleScores isn't being cleared");
		}
		System.out.println("Searching for '" + searchString + "'");

		Query query = queryParser.parse(searchString);
		TopDocs hits = indexSearcher.search(query, hitLimit);
		System.out.println("Total number of hits: " + hits.totalHits);

		ScoreDoc[] result = hits.scoreDocs;
		//File results = new File("results");
		//results.mkdir();
		//PrintWriter pw = new PrintWriter(new File("results/search record"));

		// 4. display results
		System.out.println("Found " + result.length + " hits.");
		for (int i = 0; i < result.length; ++i) {
			int docId = result[i].doc;
			Document d = indexSearcher.doc(docId);
			articleScores.put(d.get("path"), result[i].score);
	//		System.out.println(d.toString() + "\t" + result[i].score);
//			Integer.toString(rank) + "." + "\t" + //Was in the below
	//		pw.write(d.get("path") + "\n");;
		}
	//	pw.close();
		
		return articleScores;

	}

}
