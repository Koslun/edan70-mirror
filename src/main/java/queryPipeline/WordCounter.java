package queryPipeline;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Map.Entry;

public class WordCounter {
	//HashMap<AnswerCandidate, Float> saveInMe;
	private ArrayList<AnswerCandidate> answerCandidates;

	public WordCounter() {
	//	saveInMe = new HashMap<AnswerCandidate, Float>();
		answerCandidates = new ArrayList<AnswerCandidate>();
	}

	public void addAllCandidates(ArrayList<AnswerCandidate> answerCandidates){
		for(AnswerCandidate answerCandidate : answerCandidates){
			addCandidate(answerCandidate);
		}
	}

	public void addCandidate(AnswerCandidate answerCandidate){
		if(answerCandidates.contains(answerCandidate)){
			int indexOfCandidate = answerCandidates.indexOf(answerCandidate);
			AnswerCandidate alreadyExistingCandidate = answerCandidates.get(indexOfCandidate);
			alreadyExistingCandidate.addScore(answerCandidate.getScore());
		} else {
			answerCandidates.add(answerCandidate);
		}
	}

	public void countWords(String retrievedPassage, Float score){
		Scanner scan = new Scanner(retrievedPassage);
		String toParse = "";
		while (scan.hasNextLine()) {
			toParse = scan.nextLine();
			String[] tmp = toParse.split("\t");



			if (tmp.length > 1 && (tmp[3].equals("PM") || tmp[3].equals("NN"))) {
				if(score == null){
					score = 0.0f;
					System.out.println("Score was null");
				}
				AnswerCandidate answerCandidate = new AnswerCandidate(tmp[2], "", tmp[11], score);
				//System.out.println("Word(2): " + tmp[2]+ ", Classification(11): "+ tmp[11]);
				//System.out.println(toParse);
				if(answerCandidate == null){
					System.out.println("AnswerCandidate was null.");
				}

				if (answerCandidates.contains(answerCandidate)) {
					int index =  answerCandidates.indexOf(answerCandidate);
					answerCandidates.get(index).addScore(score);
				} else {
					answerCandidates.add(answerCandidate);
				}
			} else {
			//	System.out.println("Not PM or NN");
			//	System.out.println(toParse);
			}
		}
		scan.close();
	}

	public ArrayList<AnswerCandidate> baseLineSortAnswerCandidates(){
		ArrayList<AnswerCandidate> baseLineSortedAnswerCandidates = cloneAnswerCandidateList(answerCandidates);
		Collections.sort(baseLineSortedAnswerCandidates);
		return baseLineSortedAnswerCandidates;
	}

	public ArrayList<AnswerCandidate> reRankCandidates(String classification, float classificationWeight){
		for(AnswerCandidate candidate : answerCandidates){
			if(candidate.hasClassification(classification)){
				candidate.applyClassificationWeight(classificationWeight);
			} else {
				candidate.applyClassificationWeight(1.0f-classificationWeight);
			}
		}
		Collections.sort(answerCandidates);
		return answerCandidates;
	}

	private ArrayList<AnswerCandidate> cloneAnswerCandidateList(ArrayList<AnswerCandidate> answerCandidates){
		ArrayList<AnswerCandidate> answerCandidatesClone = new ArrayList<AnswerCandidate>();
		for(AnswerCandidate answerCandidate: answerCandidates){
			AnswerCandidate answerCandidateClone = answerCandidate.clone();
			answerCandidatesClone.add(answerCandidateClone);
		}
		return answerCandidatesClone;
	}

//	public void countStuff(String countMe, Float score) {
//		System.out.println("CountStuff-------------------------");
//		System.out.println(countMe);
//		Scanner scan = new Scanner(countMe);
//		String toParse = "";
//		while (scan.hasNextLine()) {
//			toParse = scan.nextLine();
//			String[] tmp = toParse.split("\t");
//
//			if (tmp.length > 1 && (tmp[3].equals("PM") || tmp[3].equals("NN"))) {
//				AnswerCandidate dd = new AnswerCandidate(tmp[2], tmp[11], 0.0f);
//				if(dd == null){
//					System.out.println("AnswerCandidate was null.");
//				}
//				if(score == null){
//					score = 0F;
//					System.out.println("Score was null");
//				}
//				if (saveInMe.containsKey(dd)) {
//					Float tmpScore = saveInMe.get(dd);
//					if(tmpScore == null){
//						tmpScore = 0F;
//						System.out.println("Needed to ini score.");
//					}
//					saveInMe.put(dd, tmpScore + score);
//				} else {
//					saveInMe.put(dd, score);
//				}
//			}
//		}
//		scan.close();
//	}
//
//	public HashMap<AnswerCandidate, Float> getStuff() {
//		return saveInMe;
//	}
//
//	public HashMap<AnswerCandidate, Float> selectCandidates(String classification) {
//		HashMap<AnswerCandidate, Float> theSelectedFew = new HashMap<AnswerCandidate, Float>();
//		for (Entry<AnswerCandidate, Float> e : saveInMe.entrySet()) {
//			if (e.getKey().getClassification().equals(classification)) {
//				theSelectedFew.put(e.getKey(), e.getValue());
//			}
//		}
//		return theSelectedFew;
//	}
}
