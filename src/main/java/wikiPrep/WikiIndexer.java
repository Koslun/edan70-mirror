package wikiPrep;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.*;
import java.util.Date;


public class WikiIndexer {


    public static void main(String[] args) throws IOException, ParseException {
        String docsSource = args[0];
        String indexPath = "../wiki_index";

        boolean create = true;

        File docDir = new File(docsSource);
        if (!docDir.exists() || !docDir.canRead()) {
            System.out
                    .println("Document directory '"
                            + docDir.getAbsolutePath()
                            + "' does not exist or is not readable, please check the path");
            System.exit(1);
        }
        Date start = new Date();
        try {
            System.out.println("Indexing " + "from '" + docsSource + "' to directory '" + indexPath + "'...");

            Directory dir = FSDirectory.open(new File(indexPath));
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(
                    Version.LUCENE_4_10_2, analyzer);

            if (create) {
                iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
            } else {
                iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
            }

            IndexWriter writer = new IndexWriter(dir, iwc);

            System.out.println("Indexing new files...");
            indexDocs(writer, docDir);
            writer.close();

            Date end = new Date();
            System.out.println("Indexing took: " + (end.getTime() - start.getTime())
                    + " milliseconds");

        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass()
                    + "\n with message: " + e.getMessage());
        }
    }

    /**
     * Indexes the given file using the given writer, or if a directory is
     * given, recurses over files and directories found under the given
     * directory.
     * <p/>
     * NOTE: This method indexes one document per input file. This is slow. For
     * good throughput, put multiple documents into your input file(s). An
     * example of this is in the benchmark module, which can create "line doc"
     * files, one document per line, using the <a href=
     * "../../../../../contrib-benchmark/org/apache/lucene/benchmark/byTask/tasks/WriteLineDocTask.html"
     * >WriteLineDocTask</a>.
     *
     * @param writer Writer to the index where the given file/dir info will be
     *               stored
     * @param file   The file to index, or the directory to recurse into to find
     *               files to index
     * @throws IOException If there is a low-level I/O error
     */
    static void indexDocs(IndexWriter writer, File file) throws IOException {
        int nbrFilesRead = 0;
        long start = System.currentTimeMillis();
        long elapsedTimeMillis = System.currentTimeMillis()-start;
        float elapsedTimeSec = elapsedTimeMillis/1000F;
        if (file.canRead()) {
            if (file.isDirectory()) {
                String[] files = file.list();
                if (files != null) {
                    for (int i = 0; i < files.length; i++) {
                        if(i % 10000 == 0){
                            System.out.println("Another 10k. At "+ i/1000 +"k.");
                            System.out.println("Indexing: " + files[i]);
                        }
                        indexDocs(writer, new File(file, files[i]));
                    }
                }
            } else {

                FileInputStream fis;
                try {
                    fis = new FileInputStream(file);
                } catch (FileNotFoundException fnfe) {
                    return;
                }
                try {
                    Document doc = new Document();

                    Field pathField = new StringField("path", file.getPath(),
                            Field.Store.YES);
                    doc.add(pathField);
                    doc.add(new LongField("modified", file.lastModified(),
                            Field.Store.NO));
                    doc.add(new TextField("contents", new BufferedReader(
                            new InputStreamReader(fis))));

                    if (writer.getConfig().getOpenMode() == IndexWriterConfig.OpenMode.CREATE) {
                     //   System.out.println("adding " + file);
                        nbrFilesRead++;
                        if(nbrFilesRead % 1000 == 0){
                            elapsedTimeMillis = System.currentTimeMillis()-start;
                            elapsedTimeSec = elapsedTimeMillis/1000F;
                            System.out.println("Added "+nbrFilesRead/1000+"k articles. Taken "+elapsedTimeSec +
                                    " seconds so far. That's about "+ ((nbrFilesRead/1280000)*100) + "%");
                        }
                        writer.addDocument(doc);
                    } else {
                        System.out.println("updating " + file);
                        writer.updateDocument(new Term("path", file.getPath()),
                                doc);
                    }
                } finally {
                    fis.close();
                }
            }
        }
    }
}
