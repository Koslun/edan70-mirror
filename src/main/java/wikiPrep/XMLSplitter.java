package wikiPrep;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class XMLSplitter {

	public static String NO_ARTICLE_NAME = "NO_ARTICLE_NAME";
	public static String NO_TITLE = "NO_TITLE";
	public static String EMPTY_LINE = "";
	
	public XMLSplitter(){}
	
	public static void main(String[] args)  {
		
		for(int i = 0; i < 10; i++){
			long start = System.currentTimeMillis(); 
			
			XMLSplitter xmlSplitter = new XMLSplitter();
			try {
				xmlSplitter.splitLines();
			} catch (IOException e) {
				System.out.println("Exception thrown when parsing.");
				e.printStackTrace();
			}

			long elapsedTimeMillis = System.currentTimeMillis()-start;
			float elapsedTimeSec = elapsedTimeMillis/1000F;
			float elapsedTimeMin = elapsedTimeMillis/(60*1000F);
			float elapsedTimeHour = elapsedTimeMillis/(60*60*1000F);
			float elapsedTimeDay = elapsedTimeMillis/(24*60*60*1000F);
			System.out.println("Elapsed time to parse 10k articles:" + elapsedTimeSec + "s");
		}
		System.out.println("Finished parsing stack of articles.");
		
	}
	
	public void splitLines() throws IOException{
		BufferedReader r;
		BufferedReader lastRow;
		PrintWriter pw;
		PrintWriter printLastRow;
		try {
			r = new BufferedReader(new FileReader("../wikidump.xml"));
			lastRow = new BufferedReader(new FileReader(
					"last_parsed_row_nbr.txt"));

			int rowsParsed = Integer.parseInt(lastRow.readLine());
			int articlesParsed = Integer.parseInt(lastRow.readLine());

			String line = EMPTY_LINE;
			String lines = EMPTY_LINE;
			String articleName = NO_ARTICLE_NAME;
			int articleLimit = 10000;
			System.out.println("Jumping to last parsed article...");
			for (int i = 0; i < rowsParsed; i++) {
				line = r.readLine();
			}

			while (r.ready() && articleLimit > 0) {
				try {

					line = r.readLine().toLowerCase();

					// System.out.println(line);

					if (line.contains("</page>")
							&& !articleName.contains(NO_ARTICLE_NAME)) {
						// System.out.println("SLUT PÅ ARTIKELN-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
						// System.out.println();

						lines += line.replaceAll("<.*?>", "");
						articleName = articleName.replaceAll("\\/", "_");
						pw = new PrintWriter("articles/" + articleName + ".txt");
						pw.write(lines);
						pw.close();

						line = EMPTY_LINE;
						lines = EMPTY_LINE;
						articleName = NO_ARTICLE_NAME;
						articleLimit--;
						articlesParsed++;

						if (articleLimit == 0) {
							printLastRow = new PrintWriter(
									"last_parsed_row_nbr.txt");
							System.out.println();
							System.out.println("Rows parsed: " + rowsParsed);
							System.out.println("Articles parsed: "
									+ articlesParsed);
							String rowsParsedString = Integer
									.toString(rowsParsed) + "\n";
							String articlesParsedString = Integer
									.toString(articlesParsed);
							printLastRow.write(rowsParsedString);
							printLastRow.write(articlesParsedString);
							printLastRow.close();
						}
					}

					if (line.contains("title=")) {
						int pos = line.indexOf("title=");
						pos += 7;
						articleName = "";
						try {
							while (line.charAt(pos) != '\"'
									&& line.charAt(pos) != '\'') {
								// while(!Pattern.matches("\\p{Punct}", "" +
								// line.charAt(pos))){
								articleName += line.charAt(pos);
								pos++;
							}
							// System.out.println(articleName);
						} catch (StringIndexOutOfBoundsException e) {
							System.out.println(line);
						}
					}

					lines += line.replaceAll("<.*?>", "") + "\n";
					rowsParsed++;

				} catch (IOException e) {
					System.out.println("Kunde inte läsa raden.");
					e.printStackTrace();
				}
			}

			if (!r.ready()) {
				System.out.println();
				System.out.println("Parsing done!");
			}

		} catch (FileNotFoundException e) {
			System.out.println("Filen hittades inte");
			e.printStackTrace();
		}
	}
}