concept	vad kallas hästhonan
concept	vad kallas den gråsvarta vårfågel som trippar över gräset med sin långa stjärt vippande
concept	vilken stor sälart lever i ishavet och har långa betar
concept	vilket rovdjur är hos oss oftast rödpälsat, har yvig svans och jagar om natten
concept	vilken gnagare har vi som husdjur och finns bl a som virvel, angora eller släthårigt
concept	vad heter de svartvita sydpolsfåglar som inte kan flyga
location	varifrån kommer sveriges enda egna ponnyras
description	vilken färg har alltid en golden retriever
concept	vilken insekt låter som om den har fler fötter än den egentligen har
concept	jordens högsta landdjur lever i afrika. vad heter det
concept	vilken långsmal gäddfisk har gröna ben
concept	vilken halvbjörn ser ut som om den har en bovmask för ögonen, och låter som den är renlig
concept	vad kallas de genomskinliga geléartade djur som kan brännas
concept	vad heter de djur som är blå på utsidan av skalet, och pärlemorskimrande på insidan
concept	vad bjöd mors lilla olle på för bär
concept	vilket barrträd brukar vi traditionellt ta in till jul
concept	vad kallas de små blodsugare som kommer i jättesvärmar och förknippas med norrland
concept	vad heter lökväxten som liknar en påsklilja och har namn efter vårhelgen pingst
concept	vilken blå blomma liknar en klocka
concept	vad heter den päronformade frukten med mörkgrönt skrovligt skal och ljusgrönt fruktkött runt en stor brun kärna
concept	på julens gottebord finns ofta dadlar samt denna torkade frukt på f
concept	vilket djur spinner gärna nät för att fånga sitt byte i
concept	en kastrerad tjur kallas så här, och är horoskopdjur i april och maj
concept	med vilken kroppsdel visar hunden vanligen att den är glad
multiplechoice	har rovfåglar bra eller dålig syn
concept	om två sorters fåglar brukar sägas att de spelar när de uppvaktar honan. den ena är orre. vilken är den andra
concept	vilken liten grävande gnagare är känd under namnet krösus i bamses värld
concept	vad heter det urgamla djur med tusentals taggar på kroppen som gärna lever i våra villaträdgårdar
concept	vad kallas en liten skogssjö med ett annat ord
concept	vad kallas det vita på träden efter en kall natt, när det inte snöat
concept	nämn tre av våra fyra årstider
location	utanför vilken svensk ö finns de berömda stenformationer som kallas raukar
location	vad kallar vi ofta en regnskog, där det finns t ex lianer
person	vad heter sveriges drottning
description	hur fick sjörövarnas fångar hoppa i havet som straff
description	vad är din mammas syster till dig
concept	vad hade riddaren på kroppen för att skydda den mot skador
location	drottning silvia är delvis uppvuxen i brasilien, men från vilket land kommer hennes familj
concept	vad kallas en stol utan ryggstöd
concept	vad kallas den sortens mössa som en tomte brukar bära
concept	vad kallas ett ärmlöst klädesplagg med knappar eller dragkedja framtill
concept	tala är silver, tiga är...
other	vilken färg har bilarna från hemglass
concept	vilken läsk med en engelsk sjua i namnet smakar fruktsoda
concept	vilken ursprunglig italiensk rätt kan heta vesuvio eller calzone
other	vilken färg har bamse på sin luva
other	vad kallas den behållare där växtdelar förmultnar
concept	vad är brago, ballerina och marie exempel på
concept	vilken är den allra billigaste och naturligaste dryck du kan dricka
concept	i aladdinasken finns både ljus och mörk choklad. vad heter asken som bara innehåller ljus choklad
concept	om man vispar ägg och vatten och steker i stekpanna, vad får man då
concept	vilket smörgåspålägg gjort av mjölk, hyvlas
person	vad heter den romerske kärleksguden som skjuter pilar
multiplechoice	hur gammal måste man vara för att få rösta i riksdagsvalet - 15, 18 eller 21 år
concept	vad kallas hårt bröd med ett annat ord
concept	vilken mejeriprodukt finns som naturell eller smaksatt med t ex vanilj och som ofta äts till frukost
concept	vilken sorts sås serveras klassiskt till äppelkaka eller rabarberpaj
concept	vad hette den största landlevande köttätande dinosaurien
other	vilket ämne gäller om du löser ekvationer och dividerar
concept	vid vilken helg brukar man av tradition äta nypotatis
concept	vilken färg har de ärtor man vanligen kokar ärtsoppa på
concept	nämn en av de två örtkryddor man brukar ha i klassisk svensk ärtsoppa
concept	delar med kniv och rosa färg
person	hon är en berättelse för små barn
other	vad heter den tecknade hästen som göra att du kan vinna på hästar, enligt reklamen
concept	vad kallas den utbildning som många tonåringar går igenom och som avslutas högtidligt i kyrkan
location	vilka länder har landsgräns mot sverige
concept	på nya zeeland heter en grön frukt och en vanlig fågel likadant. vilket är deras gemensamma namn på 4 bokstäver
description	hur ser japans flagga ut
location	i vilken stor svensk stad rinner göta älv ut
location	vad heter kinas huvudstad
organization	vad heter det stora friluftsmuseet på djurgården där det finns många djur att titta på
location	på vilken ö ligger visby
description	vad är speciellt med klaff- och vridbroar
location	vilken stad i västergötland är känd bl a för sitt populära sommarland
organization	vad kallas det hus där amerikanska presidenten bor
location	vilket landskap har en målad trähäst som souvenir
location	i vilket land finns såväl huvudstadens himmelska fridens torg som städerna kanton och shanghai
other	vilket språk är det största språket i usa
location	i vilket land finns rivieran med städer som nice och cannes
location	i vilket land i före detta jugoslavien kan besöka bl a dubrovnik
location	vilket land ligger mellan norge och finland
location	vad har haven runt polerna för gemensamt namn
location	vad kallas vi den amerikanska bergskedjan rocky mountains på svenska
location	i vilket land ligger dalslands kanal
location	vilken är portugals huvudstad
location	vad heter frankrikes huvudstad
other	vad heter sveriges nationalsång
location	vad kommer ford och chrysler ifrån
location	från vilket land kommer lagen valencia och barcelona
location	vilket land förknippar vi med såväl basebollkeps som cowboyhatt
location	i vilket land hittar man sushi och sukiyaki
location	var ligger odense
other	vilken religion har man i danmark
organization	vad heter den berömda disneyparken som ligger i florida
other	ska man säga tack i moskva säger man "spasiba". vilket språk pratar man då
location	vilket land förknippar man statsministrar som göran persson och carl bildt med
location	i vilket sydeuropeiskt semesterland, med huvudstaden madrid, är letizia prinsessa
location	i vilket land finns ormtjusare och heliga kor
person	vem sjöng den svenska vinnarlåten 2004 det gör ont
title	vad heter den musikal med abba-låtar som visats över hela världen
title	med vilken låt vann carola den internationella melodifestivalen 1991
person	vem sjunger "tjolahopp tjolahej tjolahoppsan-sa" i visan om sig själv
person	vem har under många år varit programledare för musikfrågeprogrammet så ska det låta
title	vilket bakverk har per soloskiva från 2003 namn efter
description	hur är nicke och nilla släkt med varandra
person	tillsammans med vem sjöng pernilla wahlgren låten let your spirit fly i melodifestivalen 2003
title	på vilken skiva kan simmarna skaffa husdjur
location	från vilket land kom evert taube, som skrivit många kända visor
concept	vad heter det blåsinstrument, format som en påse, som är vanligt i skottland
person	vem äger affären som bananernas i pyjamas brukar handla
timepoint	vilken dag går sista avsnittet av en julkalender på tv
concept	när musiken är lite ledsen kallas det moll. vad kallas det när musiken är glad
multiplechoice	är innehållet i en dokumentär påhittat eller verkligt
concept	vad måste man ha köpt för att kunna vinna på bingo-lotto
person	vem var programledare för expedition robinson sommaren 2004
multiplechoice	vilken av följande är inte en gladiator: amber, elektra eller gaia
person	vad heter svt:s "bildoktorn" i förnamn
person	vad heter den poptjej med efternamnet carlsson som sjungit bl a keep this fire burning och don't stop the music
organization	vad kallar sig den norrländska folkpopgrupp som gjort bl a genom eld och vatten och älvorna
location	från vilket land kommer bandet backstreet boys
person	vilken svensk kung hjälpte bellman mycket
concept	vad heter den dansform som uppförs på scen till klassisk musik, och där flickorna har tåspetsskor
concept	inuti vad brukar en cirkus uppträda
title	i vilken tv-spelsserie är den grönklädde link hjälte
organization	hej hej monika, hej på dig monika…
title	vad heter radioprogrammet där man presenterar veckans 10 mest populära svenska låtar
person	vad heter sångerskan och skådespelaren som gjort waiting for tonight, och kallas j.lo
title	i vilken film förekommer sången hakuna matata
description	var sov björnen i barnvisan björnen sover
person	vad heter pirayan som electric banan band sjunger om
multiplechoice	vilken färg brukar de tecknade myrorna i programmet ha, gröna, rosa eller gula
concept	vilken sport sysslar anja pärson med
location	vilken stad i amerikanska utah arrangerade vinter-os 2002
amount	högsta möjliga poäng för en pil i dart är 60. man har 3 pilar. vad är då högsta poäng i en omgång
concept	vad kallas den löpgren där fyra i ett lag avlöser varandra på banan
sport	vad kallas den friidrottsgren där man kastar iväg en sorts skiva
individual	vad heter den svenska golfspelerska som dominerat damgolfen i många år
other	istället för att säga att man ska bada kan man säga att man ska ta sig ett...
distance	vad är den högsta höjden i simhoppstävlingen i ett os – 8, 10 eller 12 m
sport	vad smörjer man under skidorna för att få dem att glida bättre
count	hur många personer måste man minst vara för att kunna gunga på ett gungbräde
other	vad säger man till hunden när man kastar en boll eller pinne och vill att hunden ska hämta den
count	hur många slag har man på sig att få ner käglorna
concept	vad kallas bordtennis i dagligt tal
organization	vad heter den stora engelska grästennistävlingen varje sommar
amount	hur många steg får en handbollsspelare springa utan studsa bollen
color	vilken färg är det på ordinarie speldräktens byxor
person	två svenska landlagsstjärnor vid namn hanna och fredrik har samma efternamn. vilket
count	hur många fel får man när hästen river ett hinder i hoppning
concept	vem i ishockeylaget har en plockhandske
bilar	vad kallas de små sladdlösa bilar som styrs med hjälp av en kontrollpanel som man håller i handen
verb	vad gör curlingspelarna för att stenarna ska glida bättre på isen
concept	vad kallas den typ av japansk brottning där deltagarna är extremt tjocka
concept	vad är en jamboree
concept	vad heter det spel där man ska få fem kryss eller nollor i rad på rutat papper
concept	vad kallas de 2 eller ibland 3 kort som ingår i en kortlek och föreställer en man i narrmössa
title	vilket namn har leken där man går runt stolar till musik, och det alltid fattas en stol
concept	vad kallas motionslöpning i lugnt tempo med ett ord från engelskan
concept	vad kallas issporten som är en uppvisning på skridskor
concept	vilken sport kallas soccer i usa och england
location	var befinner du dig om du tagit hissen upp till 274 meters höjd i paris
concept	vilken sport förknippar man hanna ljungberg med
location	från vilket nordiskt land kommer skidåkarna thomas alsgaard och anders aukland
concept	vilken sport sysslade wayne gretzky och leif "honken" holmkvist med
concept	om man vill ha en utskrift, vad behöver man ha kopplat till datorn då
concept	vilken fotpedal sitter normalt till höger i bilen
concept	vad kallas den typ av program som skyddar mot virus och spyware i datorn
location	vilken kontinent upptäckte vikingen leif eriksson redan år 1000, fast någon annan blivit mer känd för det
concept	vad kallas skrivtecknet som består av två prickar, den ena under den andra
concept	med vilket redskap river du t ex morötter
concept	vad kallas krigsfartyg som färdas under vattnet
concept	vad kallas den sorts verkstad där man bygger och reparerar fartyg
concept	vad kallas den säng som ambulanspersonal bär sjuka människor på
concept	vad är alvedon och treo för sorts medicin
concept	vad förpackar man i en konservburk
concept	vad går genom de stora kraftledningarna som man ofta ser på höga stolpar
concept	vilket material består ett parkettgolv av
concept	på pojkar är struphuvudet ofta större än på flickor. vad kallas det på pojkar
concept	vilket organ i kroppen tar emot luften du andas in
concept	vad kallas den person som har som jobb att laga skor
amount	hur många bilder är det vanligen på en filmrulle
concept	vad färdas passagerarna i för något när de åker luftballong
concept	vad kallas den motordrivna tvåhjuling som man får köra när man är 15 år
concept	vilka två delar till stereon innehåller vibrerande membran som gör att det blir ljud
concept	vad kallas det kort som man sätter i mobiltelefonen
concept	vad kallas en behållare där man kan slänga papper och annat skräp
concept	vad kallas den apparat som forskare använder för att se bakterier och andra minimala saker
location	vad heter planeten som också kallas aftonstjärnan och börjar på v
amount	på hur många planeter i världsrymden vet man säkert att det finns liv
timepoint	vilket år landade människan för första gången på månen - 1909, 1969 eller 1999
concept	man kan ibland se höga master med propellervingar. vad kallas de
concept	vad kallas de små energipaket man stoppar i t ex en ficklampa för att den ska lysa
amount	hur många kilo går det på ett ton
concept	vad kallas den sortens broderi där man broderar små x som tillsammans bildar mönstret
concept	vad kallar man en asfalterad väg i en stad
concept	vad är ett annat namn på en gångbana för fotgängare
concept	om man kan byta däck på bilen, kan man hissa upp den med en sådan här. vad heter den
person	vad kallas vanliga människor som inte är trollkarlar
concept	vilken sorts varelser består de arméer av, som sauron och saruman bygger upp
concept	vilken byggnad i ankeborg innehåller mest pengar
description	vilken färg på rustningen har darth vader
person	vad heter den elaka betjänten som vill ha bort katterna
person	vad heter tigern som försöker döda mowgli
location	vad heter pippis hus
person	vad heter emils mamma
person	vilka har lanthandeln på saltkråkan
person	vad heter madickens lillasyster
location	vad heter den ränna som delar mattisborgen i två delar
concept	vem är miramis
description	vad gör lotta med den jumper hon fått som hon inte gillar
concept	vad var tjoffsan för djur, som skrämde nils karlsson pyssling
concept	vad heter den stora österländska sagosamling där scheherazade berättar sagor i 1001 nätter
other	vad hade kejsaren i kejsarens nya kläder egentligen på sig
concept	vad kallas den tidningssida som sitter utanför butiker och kiosker och gör reklam för vad som står i tidningen
person	vem väckte snövit med en kyss
person	vad kallades de figurer som förr red runt i vilda västern precis som lucky luke
person	vad heter den romerska härskare som asterix ofta kämpar mot
person	vem uppfinner massor av saker
person	vad hette de två första människorna på jorden enligt bibeln
definition	vad kallas elefanternas förlängda framtänder
title	vilken tidning med avdelningen "kropp & knopp" förkortas kp
person	vad heter harrys gudfar
person	vem regisserade de tre filmerna om härskarringen
person	vem är yngst av de fyra barnen
person	vad heter den tecknade hund som gillar att sova på taket till sin hundkoja
concept	i vad sätter de sina tassavtryck i den våta cementen
person	vad kallas busarna som alltid försöker stjäla farbror joakims pengar
person	vem gillar att spränga saker
person	vad heter landet som bebos av onda människor och är fiender till narnias innevånare
person	vad heter den greve som suger blod och kan förvandla sig till en fladdermus
